#-------------------------------------------------
#
# Project created by QtCreator 2018-12-20T16:02:34
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BrainATUMtome
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#CONFIG += c++17 sdk_no_version_check

SOURCES += \
    cameradriver.cpp \
    eposdriver.cpp \
    eposwidget.cpp \
    logevents.cpp \
    logframes.cpp \
    logwidget.cpp \
        main.cpp \
        mainwindow.cpp \
    microtomedriver.cpp \
    microtomewidget.cpp \
    serialdriver.cpp \
    serialpackage.cpp \
    settings.cpp \
    syringedriver.cpp \
    syringewidget.cpp \
    viewerwidget.cpp

HEADERS += \
    cameradriver.h \
    eposdriver.h \
    eposwidget.h \
    logevents.h \
    logframes.h \
    logwidget.h \
        mainwindow.h \
    microtomedriver.h \
    microtomelayout.h \
    microtomewidget.h \
    serialdriver.h \
    serialpackage.h \
    settings.h \
    syringedriver.h \
    syringewidget.h \
    viewerwidget.h

FORMS += \
        eposwidget.ui \
        mainwindow.ui \
        microtomewidget.ui \
        syringewidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target




win32 {

    # zlib
    #LIBS += -lz

    # Visual Studio Compilers
    # vc14 Visual Studio 2015
    # vc15 Visual Studio 2017
    # vc16 Visual Studio 2019

    # eops
    LIBS += -LD:/Developer/Library/epos/win/ -lEposCmd64
    INCLUDEPATH += D:/Developer/Library/epos/win
    DEPENDPATH += D:/Developer/Library/epos/win
    EPOS_DLL=D:/Developer/Library/epos/win/EposCmd64.dll

    # opencv
    MSVC_VERSION=vc16
    OPENCV_VERSION=452
    OPENH264_VERSION=1.8.0
    OPENCV_PATH=D:/Developer/Library/opencv/$${OPENCV_VERSION}
    CONFIG(release, debug|release): LIBS += -L$${OPENCV_PATH}/x64/$${MSVC_VERSION}/lib/ -lopencv_world$${OPENCV_VERSION}
    CONFIG(debug, debug|release): LIBS += -L$${OPENCV_PATH}/x64/$${MSVC_VERSION}/lib/ -lopencv_world$${OPENCV_VERSION}d
    INCLUDEPATH += $${OPENCV_PATH}/include
    DEPENDPATH += $${OPENCV_PATH}/include

    # opencv DLLs
    #OPENCV_DLL=D:/Developer/Library/opencv/$${OPENCV_VERSION}/x64/$${MSVC_VERSION}/bin/opencv_world$${OPENCV_VERSION}.dll
    #FFMPEG_DLL=D:/Developer/Library/opencv/$${OPENCV_VERSION}/x64/$${MSVC_VERSION}/bin/opencv_videoio_ffmpeg$${OPENCV_VERSION}_64.dll
    #OPENH264_DLL=D:/Developer/Library/opencv/$${OPENCV_VERSION}/x64/$${MSVC_VERSION}/bin/openh264-$${OPENH264_VERSION}-win64.dll

    # run in release mode
    #target.path = $$OUT_PWD/release
    #target.files = $$EPOS_DLL $$OPENCV_DLL $$FFMPEG_DLL $$OPENH264_DLL
    #INSTALLS += target
}


macx {

    # zlib
    #LIBS += -lz

    # epos
    INCLUDEPATH += $$PWD/libs/epos/macx
    LIBS += -L$$PWD/libs/epos/macx -lepos

    log($$INCLUDEPATH)

    # link opencv via pkg-config
    QT_CONFIG -= no-pkg-config
    CONFIG += link_pkgconfig
    PKGCONFIG += opencv4
}


