# BrainATUMtome
control software for an ultramicrotome with an automatic slice collector

## hardware components

### ATUMtome
automated tape collecting [ultramicrotome](https://www.scienceservices.eu/atumtome-automated-tape-collecting-ultramicrotome.html)

### ultramicrotome
ultramicrotome [Leica EM UC7](https://www.leica-microsystems.com/products/sample-preparation-for-electron-microscopy/p/leica-em-uc7/)

RS232 [interface](https://gitlab.mpcdf.mpg.de/mpibr/scic/BrainATUMtome/-/blob/master/config/EMUC7_RS232Interface_OM_EN_09_16.pdf)

usb camera with [OpenCV](https://docs.opencv.org/4.5.2/d8/dfe/classcv_1_1VideoCapture.html) driver

### tape motors

tape motors [MAXON 372161](https://www.maxongroup.de/maxon/view/service_search?query=372161)

tension motor [A 3Z16-0020C](http://shop.sdp-si.com/catalog/product/?id=A%203Z16-0020C)

microcontroller [EPOS2 24/2](https://www.maxongroup.de/maxon/view/product/control/Positionierung/390438)

[EPOS2](https://www.ni.com/de-de/support/downloads/tools-network/download.epos2-positioning-controller-driver.html#374326) positioning controller driver


* maxon motors
	* BOTTOM = USB0
	* TOP = USB1

* digital outputs
	* GREEN LIGHT<br>
	USB0, 0x0003, DIC_GENERAL_PURPOSE_C, PIN13
	* SOUND<br>USB0, 0x0004, DIC_GENERAL_PURPOSE_D, PIN12
	* SUPPORT MOTORS<br>USB1, 0x0003, DIC_GENERAL_PURPOSE_C, PIN13
	* RED LIGHT<br>USB1, 0x0004, DIC_GENERAL_PURPOSE_D, PIN12



### syringe



## parameters

### microtome (UC7)
  * Cutting speed [mm/s]	0.1 – 2
  * Cutting thickness (feed) [µm]	0.025 – 300
  * Start/Stop cutting	Motor off/on
  * Counter window (nb cuts since start of series)	-
  * Total specimen advance (or advance left) [µm]	0-200 µm
  * Return speed [mm/s]	??  (before: low, medium, high), I will try to figure out the real values
  * Cutting window	Start/end of cutting window
  * N/S motor movement [µm]	Relative movement (0.1 – 500)
  * E/W motor movement [µm]	Relative movement (0.1 – 500)

### camera
  * Live-image
  * gain
  * exposure time
  * ROI
  * Snapshot
  * Video recording
  
### water pump
  * 1 button to activate pump
  * Dispersed volume/activation: ?? (I will figure it out and send you the values)

### tape mechanism
  * Tape speed: 0.1 – 2 mm/s (calibration required!!)
  * Start/stop tape motors
  * Tape tension: support motors on/off





