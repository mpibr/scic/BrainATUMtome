#include "cameradriver.h"

CameraDriver::CameraDriver(QObject *parent) : QObject(parent),
    m_videoCapture(new cv::VideoCapture()),
    m_timerAcquire(new QTimer(this))
{
    qRegisterMetaType<cv::Mat>();
    connect(m_timerAcquire, &QTimer::timeout, this, &CameraDriver::on_acquire);
}


CameraDriver::~CameraDriver()
{
    if (m_timerAcquire->isActive())
        m_timerAcquire->stop();

    if (m_videoCapture->isOpened())
        m_videoCapture->release();
}


int CameraDriver::frameWidth() const
{
    if (!m_videoCapture->isOpened())
        return 0;
    return static_cast<int>(m_videoCapture->get(cv::CAP_PROP_FRAME_WIDTH));
}


int CameraDriver::frameHeight() const
{
    if (!m_videoCapture->isOpened())
        return 0;
    return static_cast<int>(m_videoCapture->get(cv::CAP_PROP_FRAME_HEIGHT));
}


void CameraDriver::on_start(int cameraid, int frameRate)
{
    // open video capture
    m_videoCapture->open(cameraid, cv::CAP_DSHOW);
    if (!m_videoCapture->isOpened()) return;

    // start acquisition timer
    int timerInterval = 1000/frameRate;
    m_timerAcquire->start(timerInterval);
}


void CameraDriver::on_stop()
{
    if (m_timerAcquire->isActive())
        m_timerAcquire->stop();

    if (m_videoCapture->isOpened())
        m_videoCapture->release();
}


void CameraDriver::on_acquire()
{
    cv::Mat frame_cv;

    // acquire frame
    if (!m_videoCapture->read(frame_cv)) {
        on_stop();
        return;
    }
    emit frameReady(frame_cv);
}
