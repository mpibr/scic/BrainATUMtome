#ifndef CAMERADRIVER_H
#define CAMERADRIVER_H

#include <QObject>
#include <QTimer>
#include <QImage>
#include <QDebug>

#include <opencv2/opencv.hpp>

class CameraDriver : public QObject
{
    Q_OBJECT

public:
    explicit CameraDriver(QObject *parent = nullptr);
    ~CameraDriver();

    int frameWidth() const;
    int frameHeight() const;

private:
    cv::VideoCapture *m_videoCapture;
    QTimer *m_timerAcquire;

private slots:
    void on_acquire();

signals:
    void frameReady(const cv::Mat &frame);

public slots:
    void on_start(int cameraid, int frameRate);
    void on_stop();
};

Q_DECLARE_METATYPE(cv::Mat)

#endif // CAMERADRIVER_H
