#include "eposdriver.h"

EposDriver::EposDriver(QObject *parent) : QObject(parent),
    m_keyHandle_lower(nullptr),
    m_keyHandle_upper(nullptr),
    m_nodeId(1),
    m_errorCode(0)
{

}


EposDriver::~EposDriver()
{
    if (m_keyHandle_lower) {
        setLedGreen(FALSE);
        setSound(FALSE);
        stop(m_keyHandle_lower);
        close(m_keyHandle_lower);
    }

    if (m_keyHandle_upper) {
        setLedRed(FALSE);
        setTension(FALSE);
        stop(m_keyHandle_upper);
        close(m_keyHandle_upper);
    }

}


void EposDriver::open(const QString &portNameLower, const QString &portNameUpper)
{
    char deviceName[] = "EPOS2";
    char protocolStackName[] = "MAXON SERIAL V2";
    char interfaceName[] = "USB";

    // open lower motor
    m_keyHandle_lower = VCS_OpenDevice(deviceName,
                               protocolStackName,
                               interfaceName,
                               portNameLower.toUtf8().data(),
                               &m_errorCode);
    if (!m_keyHandle_lower) {
        throwError("failed to open device on port " + portNameLower);
        return;
    }
    emit notify("Epos :: device is open on port " + portNameLower);

    // open upper motor
    m_keyHandle_upper = VCS_OpenDevice(deviceName,
                               protocolStackName,
                               interfaceName,
                               portNameUpper.toUtf8().data(),
                               &m_errorCode);
    if (!m_keyHandle_upper) {
        throwError("failed to open device on port " + portNameUpper);
        return;
    }
    emit notify("Epos :: device is open on port " + portNameUpper);


    // set Velocity Mode
    setVelocityMode(m_keyHandle_lower);
    setVelocityMode(m_keyHandle_upper);
}

void EposDriver::close(HANDLE keyHandle)
{
    if (keyHandle == nullptr)
        return;

    if (!VCS_CloseDevice(keyHandle, &m_errorCode)) {
        throwError("failed to close device");
        return;
    }
}

void EposDriver::clear(HANDLE keyHandle)
{
    BOOL isFaultState = FALSE;

    if (!keyHandle)
        return;

    if (!VCS_GetFaultState(keyHandle, m_nodeId, &isFaultState, &m_errorCode)) {
        throwError("VCS_GetFaultState");
        return;
    }

    if (isFaultState) {

        if (!VCS_ClearFault(keyHandle, m_nodeId, &m_errorCode))
            throwError("VCS_ClearFault");

        // led indicator
        setLedRed(FALSE);
        setLedGreen(FALSE);
    }
}

void EposDriver::enable(HANDLE keyHandle)
{
    if (!keyHandle)
        return;

    clear(keyHandle);

    if (!VCS_SetEnableState(keyHandle, m_nodeId, &m_errorCode))
        throwError("VCS_SetEnableState");
}

void EposDriver::disable(HANDLE keyHandle)
{
    if (!keyHandle)
        return;

    if (!VCS_SetDisableState(keyHandle, m_nodeId, &m_errorCode))
        throwError("VCS_SetDisableState");
}


int EposDriver::move(HANDLE keyHandle, int velocityMust)
{
    enable(keyHandle);
    setLedGreen(FALSE);

    // stop request
    if (velocityMust == 0) {
        stop(keyHandle);
        return 0;
    }

    // check current velocity
    long velocityMustNow = 0;
    if (!VCS_GetVelocityMust(keyHandle, m_nodeId, &velocityMustNow, &m_errorCode)) {
        throwError("VCS_GetVelocityMust");
        return 0;
    }

    // stop before changing the direction
    if ((velocityMustNow >= 0) ^ (velocityMust < 0)) {
        stop(keyHandle);
        enable(keyHandle);
    }

    // set new velocity
    if (!VCS_SetVelocityMust(keyHandle, m_nodeId, velocityMust, &m_errorCode))
        throwError("VCS_SetVelocityMust");

    setLedGreen(TRUE);

    return velocityMust;
}


void EposDriver::stop(HANDLE keyHandle)
{
    if (!VCS_SetQuickStopState(keyHandle, m_nodeId, &m_errorCode))
        throwError("VCS_SetQuickStopState");

    disable(keyHandle);
}


void EposDriver::setVelocityMode(HANDLE keyHandle)
{
    if (!keyHandle)
        return;

    // set operation mode
    if (!VCS_ActivateVelocityMode(keyHandle, m_nodeId, &m_errorCode))
        throwError("VCS_ActivateVelocityMode");
}


void EposDriver::setDOPin(HANDLE keyHandle, WORD digitalOutputNb, WORD configuration, BOOL state)
{
    BOOL mask = state;
    BOOL polarity = FALSE;

    if (!keyHandle)
        return;

    if (!VCS_DigitalOutputConfiguration(keyHandle,
                                        m_nodeId,
                                        digitalOutputNb,
                                        configuration,
                                        state,
                                        mask,
                                        polarity,
                                        &m_errorCode))
        throwError("VCS_DigitalOutputConfiguration");
}


void EposDriver::setLedGreen(BOOL state)
{
    WORD digitalOutputNb = 0x0003;
    WORD configuration = DIC_GENERAL_PURPOSE_C;
    setDOPin(m_keyHandle_lower, digitalOutputNb, configuration, state);
}


void EposDriver::setSound(BOOL state)
{
    WORD digitalOutputNb = 0x0004;
    WORD configuration = DIC_GENERAL_PURPOSE_D;
    setDOPin(m_keyHandle_lower, digitalOutputNb, configuration, state);
}


void EposDriver::setTension(BOOL state)
{
    WORD digitalOutputNb = 0x0003;
    WORD configuration = DIC_GENERAL_PURPOSE_C;
    setDOPin(m_keyHandle_upper, digitalOutputNb, configuration, state);
}


void EposDriver::setLedRed(BOOL state)
{
    WORD digitalOutputNb = 0x0004;
    WORD configuration = DIC_GENERAL_PURPOSE_D;
    setDOPin(m_keyHandle_upper, digitalOutputNb, configuration, state);
}


void EposDriver::throwError(const QString &errorMessage)
{
    const WORD errorInfoSize = 256;
    char errorInfo[errorInfoSize];

    if (!VCS_GetErrorInfo(m_errorCode, errorInfo, errorInfoSize)) {
        std::string errorNote = "failed to read error information";
        std::strncpy(errorInfo, errorNote.c_str(), errorNote.length());
        errorInfo[errorNote.length()] = '\0';
    }

    emit notify("Epos :: Error, " + errorMessage + "\n" + QString(errorInfo) + ", errorCode: " + QString::number(m_errorCode, 16), Qt::red);

    // led indicator
    setLedGreen(FALSE);
    setLedRed(TRUE);
}


void EposDriver::reportVelocityChange(int velocity, const QString &tag)
{
    QString message = "Epos :: " + tag + " is ";

    if (velocity == 0) {
        message += "off";
    }
    else if (velocity < 0) {
        message += "on, direction CW, velocity " + QString::number(qAbs(velocity));
    }
    else if (velocity > 0) {
        message += "on, direction CCW, velocity " + QString::number(qAbs(velocity));
    }

    emit notify(message);
}


void EposDriver::on_velocityLowerChanged(int velocity, int direction)
{
    int velocityChanged = move(m_keyHandle_lower, velocity * direction);
    reportVelocityChange(velocityChanged, "lower motor");
}


void EposDriver::on_velocityUpperChanged(int velocity, int direction)
{
    int velocityChanged = move(m_keyHandle_upper, velocity * direction);
    reportVelocityChange(velocityChanged, "upper motor");
}


void EposDriver::on_tension(bool state)
{
    QString textState = (state) ? "on" : "off";
    emit notify("Epos :: tension motor is " + textState);
    BOOL tensionState = (state) ? TRUE : FALSE;
    setTension(tensionState);
}


void EposDriver::on_sound(bool state)
{
    QString textState = (state) ? "on" : "off";
    emit notify("Epos :: sound alarm is " + textState);
    BOOL soundState = (state) ? TRUE : FALSE;
    setSound(soundState);
}


void EposDriver::on_ledGreen(bool state)
{
    QString textState = (state) ? "on" : "off";
    emit notify("Epos :: led green is " + textState);
    BOOL ledState = (state) ? TRUE : FALSE;
    setLedGreen(ledState);
}


void EposDriver::on_ledRed(bool state)
{
    QString textState = (state) ? "on" : "off";
    emit notify("Epos :: led red is " + textState);
    BOOL ledState = (state) ? TRUE : FALSE;
    setLedRed(ledState);
}


void EposDriver::on_aboutToQuit()
{
    emit notify("Epos :: power down motors and prepare to quit");
    stop(m_keyHandle_lower);
    stop(m_keyHandle_upper);
    setTension(FALSE);
    setSound(FALSE);
    setLedGreen(FALSE);
    setLedRed(FALSE);
}
