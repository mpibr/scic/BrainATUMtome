#ifndef EPOSDRIVER_H
#define EPOSDRIVER_H

#include <QObject>

#include "Definitions.h"

class EposDriver : public QObject
{
    Q_OBJECT
public:
    explicit EposDriver(QObject *parent = nullptr);
    ~EposDriver();

    void open(const QString &portNameLower, const QString &portNameUpper);

private:
    HANDLE m_keyHandle_lower;
    HANDLE m_keyHandle_upper;
    WORD m_nodeId;
    DWORD m_errorCode;

    void close(HANDLE keyHandle);
    void clear(HANDLE keyHandle);
    void enable(HANDLE keyHandle);
    void disable(HANDLE keyHandle);
    int move(HANDLE keyHandle, int velocityMust);
    void stop(HANDLE keyHandle);

    void setVelocityMode(HANDLE keyHandle);
    void setDOPin(HANDLE keyHandle, WORD digitalOutputNb, WORD configuration, BOOL state);
    void setLedRed(BOOL state);
    void setLedGreen(BOOL state);
    void setSound(BOOL state);
    void setTension(BOOL state);
    void throwError(const QString &message);

    void reportVelocityChange(int velocity, const QString &tag);

public slots:
    void on_velocityUpperChanged(int velocity, int direction);
    void on_velocityLowerChanged(int velocity, int direction);
    void on_tension(bool state);
    void on_sound(bool state);
    void on_ledGreen(bool state);
    void on_ledRed(bool state);
    void on_aboutToQuit();

signals:
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
};

#endif // EPOSDRIVER_H
