#include "eposwidget.h"
#include "ui_eposwidget.h"

EposWidget::EposWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EposWidget),
    m_timer(new QTimer(this)),
    m_ticks(0),
    m_mmTapePerRotation(1.0)
{
    ui->setupUi(this);
    ui->spinBox_lower_velocity->setKeyboardTracking(false);
    ui->spinBox_upper_velocity->setKeyboardTracking(false);

    connect(ui->radioButton_tension_on, &QRadioButton::toggled, this, &EposWidget::tensionChanged);

    connect(ui->radioButton_sync_fwd, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsSyncToggled);
    connect(ui->radioButton_sync_rev, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsSyncToggled);
    connect(ui->radioButton_sync_off, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsSyncToggled);

    connect(ui->radioButton_upper_cw, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsUpperToggled);
    connect(ui->radioButton_upper_ccw, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsUpperToggled);
    connect(ui->radioButton_upper_off, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsUpperToggled);

    connect(ui->radioButton_lower_cw, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsLowerToggled);
    connect(ui->radioButton_lower_ccw, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsLowerToggled);
    connect(ui->radioButton_lower_off, &QRadioButton::toggled, this, &EposWidget::on_radioButtonsLowerToggled);

    connect(ui->spinBox_lower_velocity, QOverload<int>::of(&QSpinBox::valueChanged), this, &EposWidget::on_updateLowerVelocity);
    connect(ui->spinBox_upper_velocity, QOverload<int>::of(&QSpinBox::valueChanged), this, &EposWidget::on_updateUpperVelocity);

    m_timer->setInterval(1000);
    connect(m_timer, &QTimer::timeout, this, &EposWidget::on_timerTick);
}

EposWidget::~EposWidget()
{
    delete ui;
}


void EposWidget::setVelocityLower(int velocity)
{
    ui->spinBox_lower_velocity->setValue(velocity);
}


void EposWidget::setVelocityUpper(int velocity)
{
    ui->spinBox_upper_velocity->setValue(velocity);
}


void EposWidget::setMilimeterTapePerRotation(qreal value)
{
    m_mmTapePerRotation = value;
}


void EposWidget::enableButtonsGroup(bool state)
{
    ui->radioButton_lower_off->setEnabled(state);
    ui->radioButton_lower_ccw->setEnabled(state);
    ui->radioButton_lower_cw->setEnabled(state);
    ui->radioButton_upper_off->setEnabled(state);
    ui->radioButton_upper_ccw->setEnabled(state);
    ui->radioButton_upper_cw->setEnabled(state);
}


void EposWidget::on_radioButtonsSyncToggled(bool checked)
{
    if (!checked)
        return;

    if (ui->radioButton_sync_off->isChecked()) {
        // all off
        ui->radioButton_lower_off->setChecked(checked);
        ui->radioButton_upper_off->setChecked(checked);
        ui->radioButton_tension_off->setChecked(checked);
        enableButtonsGroup(true);
        m_timer->stop();
        m_ticks = 0;
    }
    else if (ui->radioButton_sync_rev->isChecked()) {
        // UPPER_CW + LOWER_CCW + TENSION_ON
        ui->radioButton_lower_ccw->setChecked(checked);
        ui->radioButton_upper_cw->setChecked(checked);
        ui->radioButton_tension_on->setChecked(checked);
        enableButtonsGroup(false);
        m_timer->start();
    }
    else if (ui->radioButton_sync_fwd->isChecked()) {
        // UPPER_CCW + LOWER_CW + TENSION_ON
        ui->radioButton_lower_cw->setChecked(checked);
        ui->radioButton_upper_ccw->setChecked(checked);
        ui->radioButton_tension_on->setChecked(checked);
        enableButtonsGroup(false);
        m_timer->start();
    }
}


void EposWidget::on_radioButtonsLowerToggled(bool checked)
{
    if (!checked)
        return;

    on_updateLowerVelocity(ui->spinBox_lower_velocity->value());
}


void EposWidget::on_radioButtonsUpperToggled(bool checked)
{
    if (!checked)
        return;

    on_updateUpperVelocity(ui->spinBox_upper_velocity->value());
}


void EposWidget::on_updateUpperVelocity(int velocity)
{
    int direction = 0;
    if (ui->radioButton_upper_ccw->isChecked()) {
        direction = 1;
    }
    else if (ui->radioButton_upper_cw->isChecked()) {
        direction = -1;
    }

    emit velocityUpperChanged(velocity, direction);
}


void EposWidget::on_updateLowerVelocity(int velocity)
{
    int direction = 0;
    if (ui->radioButton_lower_ccw->isChecked()) {
        direction = 1;
    }
    else if (ui->radioButton_lower_cw->isChecked()) {
        direction = -1;
    }

    emit velocityLowerChanged(velocity, direction);
}


void EposWidget::on_timerTick()
{
    // update elapsed time
    m_ticks++;
    QString textTime = QDateTime::fromSecsSinceEpoch(m_ticks).toUTC().toString("hh:mm:ss");
    ui->lineEdit_time_elapsed->setText(textTime);

    // update delta tape
    qreal velocity = (ui->spinBox_lower_velocity->value() + ui->spinBox_upper_velocity->value()) / 2.0;
    qreal deltaTape = velocity * (m_ticks/60.0) * m_mmTapePerRotation;
    deltaTape /= 1000; // convert mm to m
    ui->lineEdit_tape_value->setText(QString::number(deltaTape, 'f', 2));
}
