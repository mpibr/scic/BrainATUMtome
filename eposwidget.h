#ifndef EPOSWIDGET_H
#define EPOSWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QDateTime>
#include <QDebug>

namespace Ui {
class EposWidget;
}

class EposWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EposWidget(QWidget *parent = nullptr);
    ~EposWidget();

    void setVelocityLower(int velocity);
    void setVelocityUpper(int velocity);
    void setMilimeterTapePerRotation(qreal value);

private:
    Ui::EposWidget *ui;
    QTimer *m_timer;
    int m_ticks;
    qreal m_mmTapePerRotation;

    void enableButtonsGroup(bool state);

private slots:
    void on_radioButtonsSyncToggled(bool checked);
    void on_radioButtonsUpperToggled(bool checked);
    void on_radioButtonsLowerToggled(bool checked);
    void on_updateUpperVelocity(int velocity);
    void on_updateLowerVelocity(int velocity);
    void on_timerTick();

signals:
    void velocityLowerChanged(int velocity, int direction = 0);
    void velocityUpperChanged(int velocity, int direction = 0);
    void tensionChanged(bool state);
};

#endif // EPOSWIDGET_H
