#include "Definitions.h"

HANDLE VCS_OpenDevice(char deviceName[],
                      char protocolStackName[],
                      char interfaceName[],
                      char portName[],
                      DWORD *errorCode)
{
    WORD handle = DEVICE_ID;
    void *pHandle = &handle;
    std::cout << "VCS_OpenDevice" << std::endl;
    std::cout << "\t" << deviceName << std::endl;
    std::cout << "\t" << protocolStackName << std::endl;
    std::cout << "\t" << interfaceName << std::endl;
    std::cout << "\t" << portName << std::endl;
    *errorCode = 0;
    return pHandle;
}


BOOL VCS_CloseDevice(HANDLE deviceHandle, DWORD *errorCode)
{
    std::cout << "VCS_CloseDevice" << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_GetFaultState(HANDLE deviceHandle,
                       WORD nodeId,
                       BOOL *isFaultState,
                       DWORD *errorCode)
{
    std::cout << "VCS_GetFaultState " << nodeId << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
        *isFaultState = TRUE;
    }

    return TRUE;
}


BOOL VCS_GetErrorInfo(DWORD errorCode, char errorInfo[], WORD errorInfoSize)
{
    std::cout << "VCS_GetErrorInfo " << errorCode << std::endl;
    std::cout << "\t" << errorInfo << " " << errorInfoSize;

    return TRUE;
}


BOOL VCS_ClearFault(HANDLE deviceHandle, WORD nodeId, DWORD *errorCode)
{
    std::cout << "VCS_ClearFault " << nodeId << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_SetEnableState(HANDLE deviceHandle, WORD nodeId, DWORD *errorCode)
{
    std::cout << "VCS_SetEnableState " << nodeId << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_SetDisableState(HANDLE deviceHandle, WORD nodeId, DWORD *errorCode)
{
    std::cout << "VCS_SetDisableState " << nodeId << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_ActivateVelocityMode(HANDLE deviceHandle, WORD nodeId, DWORD *errorCode)
{
    std::cout << "VCS_ActivateVelocityMode " << nodeId << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_GetVelocityMust(HANDLE deviceHandle,
                         WORD nodeId,
                         long* pVelocityMust,
                         DWORD* errorCode)
{
    std::cout << "VCS_GetVelocityMust " << nodeId << std::endl;
    pVelocityMust = 0;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_SetVelocityMust(HANDLE deviceHandle, WORD nodeId, int velocity, DWORD *errorCode)
{
    std::cout << "VCS_SetVelocityMust " << nodeId << " " << velocity << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_SetQuickStopState(HANDLE deviceHandle, WORD nodeId, DWORD *errorCode)
{
    std::cout << "VCS_SetQuickStopState " << nodeId << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}


BOOL VCS_DigitalOutputConfiguration(HANDLE deviceHandle,
                                    WORD nodeId,
                                    WORD digitalOutputNb,
                                    WORD configuration,
                                    BOOL state,
                                    BOOL mask,
                                    BOOL polarity,
                                    DWORD *errorCode)
{
    std::cout << "VCS_DigitalOutputConfiguration " << nodeId << std::endl;
    std::cout << "\t" << digitalOutputNb << std::endl;
    std::cout << "\t" << configuration << std::endl;
    std::cout << "\t" << state << "\t" << mask << "\t" << polarity << std::endl;

    if (deviceHandle) {
        *errorCode = 0;
    }

    return TRUE;
}
