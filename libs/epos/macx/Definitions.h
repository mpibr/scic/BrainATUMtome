#ifndef EPOS_DEFINITIONS
#define EPOS_DEFINITIONS

#include <iostream>

typedef void *PVOID;
typedef int BOOL;
typedef PVOID HANDLE;
typedef unsigned short WORD;
typedef unsigned long DWORD;

const BOOL TRUE = 1;
const BOOL FALSE = 0;

const WORD DIC_GENERAL_PURPOSE_C = 1;
const WORD DIC_GENERAL_PURPOSE_D = 2;
const WORD DEVICE_ID = 1234;

HANDLE VCS_OpenDevice(char deviceName[],
                      char protocolStackName[],
                      char interfaceName[],
                      char portName[],
                      DWORD *errorCode);

BOOL VCS_CloseDevice(HANDLE deviceHandle, 
                     DWORD *errorCode);

BOOL VCS_GetFaultState(HANDLE deviceHandle,
                       WORD nodeId, 
                       BOOL *isFaultState, 
                       DWORD *errorCode);

BOOL VCS_GetErrorInfo(DWORD errorCode, 
                      char errorInfo[], 
                      WORD errorInfoSize);

BOOL VCS_ClearFault(HANDLE deviceHandle, 
                    WORD nodeId, 
                    DWORD *errorCode);

BOOL VCS_SetEnableState(HANDLE deviceHandle, 
                        WORD nodeId, 
                        DWORD *errorCode);

BOOL VCS_SetDisableState(HANDLE deviceHandle, 
                         WORD nodeId, 
                         DWORD *errorCode);

BOOL VCS_ActivateVelocityMode(HANDLE deviceHandle,
                              WORD nodeId,
                              DWORD *errorCode);

BOOL VCS_GetVelocityMust(HANDLE deviceHandle,
                         WORD nodeId,
                         long* pVelocityMust,
                         DWORD* errorCode);

BOOL VCS_SetVelocityMust(HANDLE deviceHandle,
                         WORD nodeId,
                         int velocity,
                         DWORD *errorCode);

BOOL VCS_SetQuickStopState(HANDLE deviceHandle,
                           WORD nodeId,
                           DWORD *errorCode);

BOOL VCS_DigitalOutputConfiguration(HANDLE deviceHandle,
                                    WORD nodeId,
                                    WORD digitalOutputNb,
                                    WORD configuration,
                                    BOOL state,
                                    BOOL mask,
                                    BOOL polarity,
                                    DWORD *errorCode);


#endif /* EPOS_DEFINITIONS */
