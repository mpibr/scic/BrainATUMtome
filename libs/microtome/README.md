# SimulateMicrotome
simulate serial communication with Leica EM UC7

## Arduino Uni
* port /dev/cu.usbmodem145101
* baud rate 19200

## ask checksum command
send a message that starts with ```FF``` and the rest of it will be packed with a checksum

## test commands with checksum
* system get part id ```!81F18E```
* system login ```!81F28D```
* system get version ```!81F58A```
* cutter motor on ```!5120018E```
* cutter motor off ```!5120008F```
* set cutting speed ```!51300199994C```
* set return speed ```!51310199994B```
* stepper commands are missing ...