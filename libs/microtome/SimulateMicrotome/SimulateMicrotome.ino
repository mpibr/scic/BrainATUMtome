// simulate Leica EM UC7

#include "serialdriver.h"
#include "microtomedriver.h"
#include "syringedriver.h"
#include "serialpackage.h"

const uint32_t baudRate = 9600;
const uint32_t bufferSize = 64;

auto hSerial = SerialDriver(bufferSize);
auto hMicrotome = MicrotomeDriver();
auto hSyringe = SyringeDriver();


void reset() {
  hSerial.reset();
  hMicrotome.reset();
  hSyringe.reset();
}


void setup() {
  hSerial.openPort(baudRate);
}


void loop() {

  // poll serial stream to receive
  if (hSerial.pollStream()) {
    hSyringe.dispatch(hSerial.package());
    hMicrotome.dispatch(hSerial.package());
  }

  // poll syringe
  if (hSyringe.respondReady()) {
    hSerial.writeRespond(hSyringe.respond());
    reset();
  }

  // poll microtome
  hMicrotome.tick();
  if (hMicrotome.respondReady()) {
    hSerial.writeRespond(hMicrotome.respond());
    reset();
  }
}
