#include "microtomedriver.h"

MicrotomeDriver::MicrotomeDriver() :
  m_respondReady(false),
  m_isCutting(false),
  m_handwheelIndex(0),
  m_timestamp(0),
  m_respond("")
{
  m_handwheelPosition[0] = Microtome::Cutter::Handwheel::retract;
  m_handwheelPosition[1] = Microtome::Cutter::Handwheel::beforeWidnow;
  m_handwheelPosition[2] = Microtome::Cutter::Handwheel::inWindow;
  m_handwheelPosition[3] = Microtome::Cutter::Handwheel::afterWindow;
}

void MicrotomeDriver::dispatch(const String &package)
{
  String message = SerialPackage::parse(package);
  
  if (SerialPackage::isTruncated(message)) {
    respondError(Microtome::System::Error::commandTruncated);
    return;
  }
    
  if (!SerialPackage::isChecksumValid(message)) {
    if (!respondChecksum(message))
      respondError(Microtome::System::Error::badChecksum);
    return;
  }

  // strip package
  uint8_t address = SerialPackage::unpack8bit(message.substring(0,2));
  uint8_t command = SerialPackage::unpack8bit(message.substring(2,4));
  String subpackage = message.substring(4, message.length() - 2); // remove checksum

  // parse address
  switch (address) {
    case Microtome::System::address:
      parseSystem(command, subpackage);
      break;
    case Microtome::Stepper::address:
      parseStepper(command, subpackage);
      break;
    case Microtome::Cutter::address:
      parseCutter(command, subpackage);
      break;
    default:
      respondError(Microtome::System::Error::unknownCommand);
  }
}


void MicrotomeDriver::tick()
{
  if (!m_isCutting)
    return;
  
  uint32_t delta_time = micros() - m_timestamp;
  const uint32_t delta_interval = 1000000; // 1 second
  if (delta_time > delta_interval) {
    m_handwheelIndex++;
    if (m_handwheelIndex >= 4)
      m_handwheelIndex = 0;

    String message = SerialPackage::pack8bit(Microtome::Cutter::respond) +
                     SerialPackage::pack8bit(Microtome::Cutter::handwheelPosition) +
                     SerialPackage::pack8bit(Microtome::Status::get) +
                     SerialPackage::pack8bit(m_handwheelPosition[m_handwheelIndex]);
    m_respond = SerialPackage::package(message);
    m_respondReady = true;
    m_timestamp = micros();
  }
  
}


void MicrotomeDriver::reset()
{
  m_respondReady = false;
  m_respond = "";
}


void MicrotomeDriver::parseSystem(uint8_t command, const String &package)
{
  String message = SerialPackage::pack8bit(Microtome::System::respond);
  
  switch (command) {
    case Microtome::System::getPartId:
      message = message + SerialPackage::pack8bit(Microtome::System::getPartId) +
                SerialPackage::pack8bit(0x01) +
                SerialPackage::pack16bit(0x2345) +
                SerialPackage::pack16bit(0x6789);
      break;
    case Microtome::System::login:
      message = message + SerialPackage::pack8bit(Microtome::System::login);
      break;
    case Microtome::System::getVersion:
      message = message + SerialPackage::pack8bit(Microtome::System::getVersion) +
                SerialPackage::pack8bit(0x11) +
                SerialPackage::pack8bit(0x22) +
                SerialPackage::pack8bit(0x33);
      break;
    default:
      respondError(Microtome::System::Error::unknownCommand);
      return;
  }
  m_respond = SerialPackage::package(message);
  m_respondReady = true;
}


void MicrotomeDriver::parseStepper(uint8_t command, const String &package)
{
  String message = SerialPackage::pack8bit(Microtome::Stepper::respond);
}


void MicrotomeDriver::parseCutter(uint8_t command, const String &package)
{
  String message = SerialPackage::pack8bit(Microtome::Cutter::respond);

  switch(command) {
    case Microtome::Cutter::motor:
      parseCutterMotor(package);
      message = message +
                SerialPackage::pack8bit(Microtome::Cutter::motor) +
                SerialPackage::pack8bit(Microtome::Status::get) +
                package;
      break;
    case Microtome::Cutter::cuttingSpeed:
      message = message +
                SerialPackage::pack8bit(Microtome::Cutter::cuttingSpeed) +
                SerialPackage::pack8bit(Microtome::Status::get) +
                package.substring(2, package.length());
      break;
    case Microtome::Cutter::returnSpeed:
      message = message +
                SerialPackage::pack8bit(Microtome::Cutter::returnSpeed) +
                SerialPackage::pack8bit(Microtome::Status::get) +
                package.substring(2, package.length());
      break;
    case Microtome::Cutter::handwheelPosition:
      break;
    default:
      respondError(Microtome::System::Error::unknownCommand);
      return;
  }

  m_respond = SerialPackage::package(message);
  m_respondReady = true;
}


void MicrotomeDriver::parseCutterMotor(const String &package)
{
  uint8_t code = SerialPackage::unpack8bit(package);
  if (code == Microtome::Status::nll) {
    m_isCutting = false;
  }
  else if (code == Microtome::Status::set) {
    m_isCutting = true;
    m_timestamp = micros();
  }
}


bool MicrotomeDriver::respondChecksum(const String &package)
{
  uint8_t code = SerialPackage::unpack8bit(package.substring(0, 2));
  if (code != 0xFF)
    return false;
    
  m_respond = SerialPackage::package(package.substring(2, package.length()));
  m_respondReady = true;
  
  return true;
}


void MicrotomeDriver::respondError(uint8_t code)
{
  String message = SerialPackage::pack8bit(Microtome::System::respond) +
                   SerialPackage::pack8bit(Microtome::System::Error::command) +
                   SerialPackage::pack8bit(code);
  m_respond = SerialPackage::package(message);
  m_respondReady = true;
}
