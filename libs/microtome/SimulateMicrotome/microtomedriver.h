#ifndef MICROTOMEDRIVER_H
#define MICROTOMEDRIVER_H

#include "Arduino.h"
#include "microtomelayout.h"
#include "serialpackage.h"

class MicrotomeDriver
{
public:
  explicit MicrotomeDriver();

  void dispatch(const String &package);
  void tick();
  void reset();
  bool respondReady() const {return m_respondReady;}
  String respond() const {return m_respond;}

private:
  bool m_respondReady;
  bool m_isCutting;
  uint8_t m_handwheelIndex;
  uint32_t m_timestamp;
  String m_respond;
  uint8_t m_handwheelPosition[4];
  
  void parseSystem(uint8_t command, const String &package);
  void parseStepper(uint8_t command, const String &package);
  void parseCutter(uint8_t command, const String &package);
  void parseCutterMotor(const String &package);
  bool respondChecksum(const String &package);
  void respondError(uint8_t code);
};

#endif // MICROTOMEDRIVER_H
