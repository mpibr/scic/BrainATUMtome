#ifndef MICROTOMELAYOUT_H
#define MICROTOMELAYOUT_H

#include <stdint.h>

namespace Microtome {

    namespace System {
        const uint8_t address = 0x81;
        const uint8_t respond = 0x18;
        const uint8_t softwareReset = 0xF0;
        const uint8_t getPartId = 0xF1;
        const uint8_t login = 0xF2;
        namespace Error {
            const uint8_t command = 0xF3;
            const uint8_t bufferOverflow = 0x00;
            const uint8_t unknownCommand = 0x03;
            const uint8_t badParameter = 0x07;
            const uint8_t badChecksum = 0x0C;
            const uint8_t commandTruncated = 0x0D;
        }
        const uint8_t getVersion = 0xF5;
    }

    namespace Stepper {
        const uint8_t address = 0x41;
        const uint8_t respond = 0x14;

        namespace Feedrate {
            const uint8_t motorControl = 0x20;
            const uint8_t sendPosition = 0x21;
            const uint8_t feed = 0x23;
        }

        namespace NorthSouthStage {
            const uint8_t motorControl = 0x30;
            const uint8_t sendPosition = 0x31;
        }

        namespace EastWestStage {
            const uint8_t motorControl = 0x40;
            const uint8_t sendPosition = 0x41;
        }
    }

    namespace Cutter {
        const uint8_t address = 0x51;
        const uint8_t respond = 0x15;
        const uint8_t motor = 0x20;
        const uint8_t cuttingSpeed = 0x30;
        const uint8_t returnSpeed = 0x31;
        const uint8_t handwheelPosition = 0x40;

        namespace Handwheel {
            const uint8_t retract = 0x00;
            const uint8_t beforeWidnow = 0x01;
            const uint8_t afterWindow = 0x02;
            const uint8_t inWindow = 0x03;
        }
    }

    namespace Status {
        const uint8_t nll = 0x00;
        const uint8_t set = 0x01;
        const uint8_t get = 0xFF;
        const uint8_t invalidCalibration = 0xE0;
    }

}



#endif // MICROTOMELAYOUT_H
