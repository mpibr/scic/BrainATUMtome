#ifndef SERIALDRIVER_H
#define SERIALDRIVER_H

#include "Arduino.h"

class SerialDriver
{
public:
  explicit SerialDriver(uint32_t buffersize);
  void openPort(uint32_t baudrate);
  void writeRespond(const String &respond);
  bool pollStream();
  String package() const {return m_bufferPackage;}
  void reset();
  
private:
  bool m_isReadyRead;
  String m_bufferRead;
  String m_bufferPackage;
};

#endif // SERIALDRIVER_H
