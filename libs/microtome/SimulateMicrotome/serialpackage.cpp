#include "serialpackage.h"

bool SerialPackage::isTruncated(const String &buffer)
{
  return (buffer.length() % 2 != 0);
}


bool SerialPackage::isChecksumValid(const String &buffer)
{
  uint8_t checksumReceived = unpack8bit(buffer.substring(buffer.length() - 2, buffer.length()));
  uint8_t checksumLocal = checksum(buffer.substring(0, buffer.length() - 2));
  return (checksumReceived == checksumLocal);
}


uint8_t SerialPackage::checksum(const String &buffer)
{
  uint8_t valueChecksum = 0;
  for (int i = 0; i < buffer.length(); i += 2)
    valueChecksum += unpack8bit(buffer.substring(i, i + 2));
  valueChecksum = ~valueChecksum + 1;
  return valueChecksum;  
}


uint8_t SerialPackage::unpack8bit(const String &buffer)
{
  return static_cast<uint8_t>(strtoul(buffer.c_str(), NULL, 16));
}


uint16_t SerialPackage::unpack16bit(const String &buffer)
{
  return static_cast<uint16_t>(strtoul(buffer.c_str(), NULL, 16));
}


uint32_t SerialPackage::unpack32bit(const String &buffer)
{
  return static_cast<uint32_t>(strtoul(buffer.c_str(), NULL, 16));
}


String  SerialPackage::parse(const String &buffer)
{
  String local = "";
  for (int i = 0; i < buffer.length(); i++) {
    char key = buffer.charAt(i);
    if ((('0' <= key) && (key <= '9')) ||
        (('A' <= key) && (key <= 'F')) ||
        (('a' <= key) && (key <= 'f')))
        local = String(local + key);
  }
  return local;
}


String  SerialPackage::package(const String &buffer)
{
  uint8_t csvalue = checksum(buffer);
  String cstext = pack8bit(csvalue);
  return String("!" + buffer + cstext + "\r");
}


String  SerialPackage::pack8bit(uint8_t value)
{
  char buffer[3];
  sprintf(buffer, "%02X", value);
  return String(buffer);
}


String  SerialPackage::pack16bit(uint16_t value)
{
  char buffer[5];
  sprintf(buffer, "%04X", value);
  return String(buffer);
}


String  SerialPackage::pack32bit(uint32_t value)
{
  char buffer[9];
  sprintf(buffer, "%08lX", value);
  return String(buffer);
}
