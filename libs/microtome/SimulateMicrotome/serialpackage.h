#ifndef SERIALPACKAGE_H
#define SERIALPACKAGE_H

#include "Arduino.h"

namespace SerialPackage
{
  bool isTruncated(const String &buffer);
  bool isChecksumValid(const String &buffer);
  uint8_t checksum(const String &buffer);
  uint8_t unpack8bit(const String &buffer);
  uint16_t unpack16bit(const String &buffer);
  uint32_t unpack32bit(const String &buffer);
  String  parse(const String &buffer);
  String  package(const String &buffer);
  String  pack8bit(uint8_t value);
  String  pack16bit(uint16_t value);
  String  pack32bit(uint32_t value);
}

#endif // SERIALPACKAGE_H
