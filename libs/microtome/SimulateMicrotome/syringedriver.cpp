#include "syringedriver.h"

SyringeDriver::SyringeDriver() :
  m_respondReady(false),
  m_respond("")
{
}


void SyringeDriver::dispatch(const String &package)
{
  if (package == "VER")
    addRespond("VER1234");
  else if (package == "RUN")
    addRespond("RUNSTART");
  else if (package == "STP")
    addRespond("DONE");
}


void SyringeDriver::addRespond(const String &respond)
{
  m_respondReady = true;
  m_respond = respond;
}


void SyringeDriver::reset()
{
  m_respondReady = false;
  m_respond = "";
}
