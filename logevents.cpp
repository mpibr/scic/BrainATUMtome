#include "logevents.h"

LogEvents::LogEvents(QObject *parent) : QObject(parent),
    m_path(QDir(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).filePath("logs")),
    m_tag("LogEvents")
{

}


void LogEvents::open(const QString &path, const QString &tag)
{
    setPath(path);
    setTag(tag);
    open();
}


void LogEvents::open()
{
    QString timeStamp = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss");
    QString fileName = "EventLog_" + timeStamp + "_" + m_tag + ".txt";

    QDir pathExport = QDir(m_path);
    if (!pathExport.exists())
        pathExport.mkdir(m_path);

    m_logFile.setFileName(pathExport.filePath(fileName));
    m_logFile.open(QIODevice::WriteOnly | QIODevice::Text);
    //m_logFile.open(QIODevice::WriteOnly);
    if (!m_logFile.isOpen() || !m_logFile.isWritable()) {
        QString errorMessage = "LogEvents :: error, failed to open log file " + m_logFile.fileName();
        emit notify(errorMessage, Qt::red);
        return;
    }
}


void LogEvents::close()
{
    if (m_logFile.isOpen())
        m_logFile.close();
}


void LogEvents::on_enable(bool state)
{
    if (state)
        open();
    else
        close();
}


void LogEvents::on_log(const QString &message)
{
    if (!(m_logFile.isOpen() && m_logFile.isWritable()))
        return;

    QString timeStamp = QDateTime::currentDateTime().toString("[yyyyMMdd hh:mm:ss.zzz]");
    QString logMessage = timeStamp + " " + message;

    // write zlib
    //QByteArray logBuffer = qCompress(logMessage.toUtf8());
    //logBuffer.remove(0, 4); // qCompress prepend 4 non-standard bytes for message length (32bit integer)
    //m_logFile.write(logBuffer);

    // write text
    QTextStream logStream(&m_logFile);
    logStream << logMessage << "\n";
}

