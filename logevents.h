#ifndef LOGEVENTS_H
#define LOGEVENTS_H

#include <QObject>
#include <QDateTime>
#include <QDir>
#include <QFile>
//#include <QByteArray>
#include <QTextStream>
#include <QStandardPaths>


class LogEvents : public QObject
{
    Q_OBJECT

public:
    explicit LogEvents(QObject *parent = nullptr);
    void open(const QString &path, const QString &tag);
    void open();
    void close();
    void setPath(const QString &path) {m_path = path;}
    void setTag(const QString &tag) {m_tag = tag;}

private:
    QString m_path;
    QString m_tag;
    QFile m_logFile;

public slots:
    void on_enable(bool state);
    void on_log(const QString &message);

signals:
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
};

#endif // LOGEVENTS_H
