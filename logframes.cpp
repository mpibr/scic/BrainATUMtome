#include "logframes.h"

LogFrames::LogFrames(QObject *parent) : QObject(parent),
    m_isSnapping(false),
    m_frameIndex(0),
    m_sections(0),
    m_advance(0),
    m_fps(10.0),
    m_frameSize(cv::Size(512, 512)),
    m_path(QDir(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).filePath("frames")),
    m_tag("LogFrames")
{

}


void LogFrames::on_enable(bool state)
{
    if (state) {
        m_frameIndex = 0;

        QString timeStamp = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss");
        QString videoName = QDir(m_path).filePath("Video_" + timeStamp + "_" + m_tag + ".mp4");
        int codec = cv::VideoWriter::fourcc('H', '2', '6', '4');
        m_writer.open(videoName.toStdString(), codec, m_fps, m_frameSize);
        if (!m_writer.isOpened()) {
            QString errorMessage = "LogFrames :: error, failed to open log file " + videoName;
            emit notify(errorMessage, Qt::red);
            return;
        }
    }
    else {
        if (m_writer.isOpened())
            m_writer.release();
    }
}


void LogFrames::on_snap()
{
    m_isSnapping = true;
}


void LogFrames::on_frame(const cv::Mat &frame)
{
    m_frameSize = frame.size();

    if (m_isSnapping) {
        QString timeStamp = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss");
        QString imageName = QDir(m_path).filePath("Snap_" + timeStamp + "_" + m_tag + ".jpg");
        cv::imwrite(imageName.toStdString(), frame);
        emit notify("LogFrames :: snap");
        m_isSnapping = false;
    }


    if (m_writer.isOpened()) {

        cv::Mat frameNow;
        frame.copyTo(frameNow);

        /*
        QString stampTime = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss.zzz");
        QString stampFrame = QString::number(m_frameIndex);
        QString stampSection = QString::number(m_sections);
        QString stampAdvance = QString::number(m_advance);

        cv::putText(frameNow, stampTime.toStdString(), cv::Point(5, 15), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 0, 0));
        cv::putText(frameNow, stampFrame.toStdString(), cv::Point(5, 35), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 0, 0));
        cv::putText(frameNow, stampSection.toStdString(), cv::Point(5, 55), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 0, 0));
        cv::putText(frameNow, stampAdvance.toStdString(), cv::Point(5, 75), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 0, 0));
        */
        m_writer.write(frameNow);
        m_frameIndex++;
        emit frameLogged("LogFrames :: frame " + QString::number(m_frameIndex));
    }

}


void LogFrames::on_sectionsChanged(int sections)
{
    m_sections = sections;
}


void LogFrames::on_advancedChanged(int advance)
{
    m_advance = advance;
}
