#ifndef LOGFRAMES_H
#define LOGFRAMES_H

#include <QObject>
#include <QDateTime>
#include <QDir>
#include <QStandardPaths>
#include <opencv2/opencv.hpp>

class LogFrames : public QObject
{
    Q_OBJECT
public:
    explicit LogFrames(QObject *parent = nullptr);

    void setPath(const QString &path) {m_path = path;}
    void setTag(const QString &tag) {m_tag = tag;}
    void setFrameRate(double fps) {m_fps = fps;}

private:
    bool m_isSnapping;
    int m_frameIndex;
    int m_sections;
    int m_advance;
    double m_fps;
    cv::Size m_frameSize;
    QString m_path;
    QString m_tag;
    cv::VideoWriter m_writer;

public slots:
    void on_frame(const cv::Mat &frame);
    void on_snap();
    void on_enable(bool state);
    void on_sectionsChanged(int sections);
    void on_advancedChanged(int advance);

signals:
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
    void frameLogged(const QString &tag);

};

#endif // LOGFRAMES_H
