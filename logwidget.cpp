#include "logwidget.h"

LogWidget::LogWidget(QWidget *parent) : QPlainTextEdit (parent)
{
    qRegisterMetaType<Qt::GlobalColor>();
}


void LogWidget::on_notify(const QString &message, const Qt::GlobalColor &color)
{
    QTextCharFormat textFormat = this->currentCharFormat();
    textFormat.setForeground(QBrush(QColor(color), Qt::SolidPattern));
    this->mergeCurrentCharFormat(textFormat);
    this->appendPlainText(message);
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->maximum());
}
