#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_timerHandshake(new QTimer(this)),
    m_settings(new Settings(this)),
    m_eposDriver(new EposDriver(this)),
    m_microtomeDriver(new MicrotomeDriver(this)),
    m_syringeDriver(new SyringeDriver(this)),
    m_cameraDriver(new CameraDriver()),
    m_threadCameraDriver(new QThread(this)),
    m_logEvents(new LogEvents()),
    m_threadLogEvents(new QThread(this)),
    m_logFrames(new LogFrames()),
    m_threadLogFrames(new QThread(this))
{
    ui->setupUi(this);

    // enable notifications
    connect(this, &MainWindow::notify, ui->logWidget, &LogWidget::on_notify);
    connect(m_settings, &Settings::notify, this, &MainWindow::notify);
    connect(m_eposDriver, &EposDriver::notify, this, &MainWindow::notify);
    connect(m_microtomeDriver, &MicrotomeDriver::notify, this, &MainWindow::notify);
    connect(m_syringeDriver, &SyringeDriver::notify, this, &MainWindow::notify);
    connect(m_logEvents, &LogEvents::notify, this, &MainWindow::notify);
    connect(m_logFrames, &LogFrames::notify, this, &MainWindow::notify);

    // enable quit
    connect(this, &MainWindow::aboutToQuit, m_eposDriver, &EposDriver::on_aboutToQuit);
    connect(this, &MainWindow::aboutToQuit, m_microtomeDriver, &MicrotomeDriver::on_aboutToQuit);
    connect(this, &MainWindow::aboutToQuit, m_cameraDriver, &CameraDriver::on_stop);


    // setup environment
    m_settings->open("settings.txt");
    setupEpos();
    setupMicrotome();
    setupSyringe();
    setupCamera();
    setupLoggerEvents();
    setupLoggerFrames();

    // handshake
    connect(m_timerHandshake, &QTimer::timeout, [this](){ emit notify("BrainATUMtome v2", Qt::gray); });
    connect(m_timerHandshake, &QTimer::timeout, m_microtomeDriver, &MicrotomeDriver::on_handshake);
    connect(m_timerHandshake, &QTimer::timeout, m_syringeDriver, &SyringeDriver::on_handshake);
    m_timerHandshake->setSingleShot(true);
    m_timerHandshake->start(2000);
}

MainWindow::~MainWindow()
{ 
    if (m_threadCameraDriver) {
        m_threadCameraDriver->quit();
        m_threadCameraDriver->wait();
    }

    if (m_threadLogEvents) {
        m_threadLogEvents->quit();
        m_threadLogEvents->wait();
    }

    if (m_threadLogFrames) {
        m_threadLogFrames->quit();
        m_threadLogFrames->wait();
    }

    delete ui;
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    if (QMessageBox::Yes == QMessageBox::question(this, "Close application", "Exit?", QMessageBox::Yes | QMessageBox::No)) {
        emit aboutToQuit();
        event->accept();
    }
}


void MainWindow::setupEpos()
{
    ui->eposWidget->setVelocityLower(m_settings->eposLowerVelocity());
    ui->eposWidget->setVelocityUpper(m_settings->eposUpperVelocity());
    connect(ui->eposWidget, &EposWidget::velocityLowerChanged, m_settings, &Settings::on_lowerVelocityChanged);
    connect(ui->eposWidget, &EposWidget::velocityUpperChanged, m_settings, &Settings::on_upperVelocityChanged);
    connect(ui->eposWidget, &EposWidget::velocityLowerChanged, m_eposDriver, &EposDriver::on_velocityLowerChanged);
    connect(ui->eposWidget, &EposWidget::velocityUpperChanged, m_eposDriver, &EposDriver::on_velocityUpperChanged);
    connect(ui->eposWidget, &EposWidget::tensionChanged, m_eposDriver, &EposDriver::on_tension);
    m_eposDriver->open(m_settings->eposLowerPort(), m_settings->eposUpperPort());
}


void MainWindow::setupMicrotome()
{
    ui->microtomeWidget->setCuttingSpeed(m_settings->microtomeCuttingSpeed());
    ui->microtomeWidget->setReturnSpeed(m_settings->microtomeReturnSpeed());
    ui->microtomeWidget->setFeedRate(m_settings->microtomeFeedStep());
    ui->microtomeWidget->setSections(m_settings->microtomeSections(), 0);
    ui->microtomeWidget->setAdvance(m_settings->microtomeAdvance(), 0, m_settings->microtomeFeedRateTotal());
    connect(ui->microtomeWidget, &MicrotomeWidget::cuttingSpeedChanged, m_settings, &Settings::on_cuttingSpeedChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::returnSpeedChanged, m_settings, &Settings::on_returnSpeedChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::feedRateChanged, m_settings, &Settings::on_feedStepChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::sectionsChanged, m_settings, &Settings::on_sectionsChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::advanceChanged, m_settings, &Settings::on_advanceChanged);

    connect(ui->microtomeWidget, &MicrotomeWidget::cuttingSpeedChanged, m_microtomeDriver, &MicrotomeDriver::on_cuttingSpeedChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::returnSpeedChanged, m_microtomeDriver, &MicrotomeDriver::on_returnSpeedChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::feedRateChanged, m_microtomeDriver, &MicrotomeDriver::on_feedRateChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::cuttingMotor, m_microtomeDriver, &MicrotomeDriver::on_cuttingMotorStateChanged);

    connect(m_microtomeDriver, &MicrotomeDriver::sectionDone, ui->microtomeWidget, &MicrotomeWidget::on_sectionsChannged);
    connect(m_microtomeDriver, &MicrotomeDriver::advancePosition, ui->microtomeWidget, &MicrotomeWidget::on_advanceChanged);
    connect(m_microtomeDriver, &MicrotomeDriver::handwheelPositionChanged, ui->microtomeWidget, &MicrotomeWidget::on_handwheelPositionChanged);
    connect(m_microtomeDriver, &MicrotomeDriver::cuttingMotorStateChanged, ui->microtomeWidget, &MicrotomeWidget::on_cuttingMotorStateChanged);
    m_microtomeDriver->open(m_settings->microtomePort(), m_settings->microtomeBaudRate());
}


void MainWindow::setupSyringe()
{
    m_syringeDriver->open(m_settings->syringePort(), m_settings->syringeBaudRate());
    connect(ui->microtomeWidget, &MicrotomeWidget::sectionsChanged, ui->syringeWidget, &SyringeWidget::on_sectionsChannged);
    connect(ui->syringeWidget, &SyringeWidget::pump, m_syringeDriver, &SyringeDriver::on_pump);
}


void MainWindow::setupCamera()
{
    m_cameraDriver->moveToThread(m_threadCameraDriver);
    connect(m_threadCameraDriver, &QThread::finished, m_cameraDriver, &CameraDriver::deleteLater);
    m_threadCameraDriver->start();
    connect(this, &MainWindow::cameraStart, m_cameraDriver, &CameraDriver::on_start);
    connect(this, &MainWindow::cameraStop, m_cameraDriver, &CameraDriver::on_stop);
    connect(m_cameraDriver, &CameraDriver::frameReady, ui->viewerWidget, &ViewerWidget::on_frame, Qt::QueuedConnection);
    emit cameraStart(m_settings->cameraId(), m_settings->cameraFrameRate());
}


void MainWindow::setupLoggerEvents()
{
    m_logEvents->setPath(m_settings->pathLogs());
    m_logEvents->setTag("BrainATUMtome");
    connect(ui->microtomeWidget, &MicrotomeWidget::enableEventsLog, m_logEvents, &LogEvents::on_enable);
    connect(this, &MainWindow::notify, m_logEvents, &LogEvents::on_log);

    m_logEvents->moveToThread(m_threadLogEvents);
    connect(m_threadLogEvents, &QThread::finished, m_logEvents, &LogEvents::deleteLater);
    m_threadLogEvents->start();
}


void MainWindow::setupLoggerFrames()
{
    m_logFrames->setPath(m_settings->pathVideos());
    m_logFrames->setTag("BrainATUMtome");
    m_logFrames->setFrameRate(m_settings->cameraFrameRate());
    connect(m_cameraDriver, &CameraDriver::frameReady, m_logFrames, &LogFrames::on_frame);
    connect(ui->microtomeWidget, &MicrotomeWidget::snap, m_logFrames, &LogFrames::on_snap);
    connect(ui->microtomeWidget, &MicrotomeWidget::enableVideoLog, m_logFrames, &LogFrames::on_enable);
    connect(m_logFrames, &LogFrames::frameLogged, m_logEvents, &LogEvents::on_log);
    connect(ui->microtomeWidget, &MicrotomeWidget::sectionsChanged, m_logFrames, &LogFrames::on_sectionsChanged);
    connect(ui->microtomeWidget, &MicrotomeWidget::advanceChanged, m_logFrames, &LogFrames::on_advancedChanged);

    m_logFrames->moveToThread(m_threadLogFrames);
    connect(m_threadLogFrames, &QThread::finished, m_logFrames, &LogFrames::deleteLater);
    m_threadLogFrames->start();
}
