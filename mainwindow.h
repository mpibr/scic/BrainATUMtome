#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QThread>
#include <QCloseEvent>
#include <QMessageBox>
#include <QDebug>

#include "settings.h"
#include "cameradriver.h"

#include "viewerwidget.h"
#include "logwidget.h"
#include "logevents.h"
#include "logframes.h"

#include "eposwidget.h"
#include "eposdriver.h"

#include "microtomewidget.h"
#include "microtomedriver.h"

#include "syringewidget.h"
#include "syringedriver.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTimer *m_timerHandshake;
    Settings *m_settings;
    EposDriver *m_eposDriver;
    MicrotomeDriver *m_microtomeDriver;
    SyringeDriver *m_syringeDriver;
    CameraDriver *m_cameraDriver;
    QThread *m_threadCameraDriver;
    LogEvents *m_logEvents;
    QThread *m_threadLogEvents;
    LogFrames *m_logFrames;
    QThread *m_threadLogFrames;

    void closeEvent(QCloseEvent *event) override;

    void setupEpos();
    void setupMicrotome();
    void setupSyringe();
    void setupCamera();
    void setupLoggerEvents();
    void setupLoggerFrames();

signals:
    void aboutToQuit();
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
    void cameraStart(int cameraid, int frameRate);
    void cameraStop();
};

#endif // MAINWINDOW_H
