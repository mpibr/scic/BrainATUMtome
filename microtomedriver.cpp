#include "microtomedriver.h"

MicrotomeDriver::MicrotomeDriver(QObject *parent) : QObject(parent),
    m_serialDriver(new SerialDriver(this))
{
    connect(this, &MicrotomeDriver::schedule, m_serialDriver, &SerialDriver::on_schedule);
    connect(m_serialDriver, &SerialDriver::readyPackage, this, &MicrotomeDriver::on_readyPackage);
    connect(m_serialDriver, &SerialDriver::notify, this, &MicrotomeDriver::notify);
}


void MicrotomeDriver::open(const QString &port, int baudrate)
{
    m_serialDriver->open(port, baudrate);
    m_serialDriver->setPackageEnd('\r');
}

void MicrotomeDriver::parseSystemRespond(const QString &package)
{
    quint8 command = SerialPackage::unpack8bit(package.mid(0, 2));
    quint8 hardwareRevision;
    quint8 majorRevision;
    quint8 minorRevision;

    switch (command) {
    case Microtome::System::softwareReset:
        reportStatus("system software reset");
        break;
    case Microtome::System::getPartId:
        reportStatus("system part id " + package.mid(2, 10));
        break;
    case Microtome::System::login:
        reportStatus("login confirmed");
        break;
    case Microtome::System::Error::command:
        parseSystemErrorRespond(package.mid(2,2));
        break;
    case Microtome::System::getVersion:
        hardwareRevision = SerialPackage::unpack8bit(package.mid(2, 2));
        majorRevision = SerialPackage::unpack8bit(package.mid(4, 2));
        minorRevision = SerialPackage::unpack8bit(package.mid(6, 2));
        reportStatus("system version " +
                    QString::number(hardwareRevision) + "." +
                    QString::number(majorRevision) + "." +
                    QString::number(minorRevision));
        break;
    default:
        reportError("system respond is invalid");
    }

}


void MicrotomeDriver::parseSystemErrorRespond(const QString &package)
{
    quint8 code = SerialPackage::unpack8bit(package.mid(0, 2));
    switch (code) {
    case Microtome::System::Error::bufferOverflow:
        reportError("system buffer overflow");
        break;
    case Microtome::System::Error::unknownCommand:
        reportError("system unknown command");
        break;
    case Microtome::System::Error::badParameter:
        reportError("system bad parameter");
        break;
    case Microtome::System::Error::badChecksum:
        reportError("system bad checksum");
        break;
    case Microtome::System::Error::commandTruncated:
        reportError("system command is truncated");
        break;
    default:
        reportError("system respond is invalid " + package);
    }
}


void MicrotomeDriver::parseStepperRespond(const QString &package)
{
    quint8 command = SerialPackage::unpack8bit(package.mid(0, 2));
    //quint8 code = SerialPackage::unpack8bit(package.mid(2, 2));

    quint16 feedstep = SerialPackage::unpack16bit(package.mid(4, 4));
    quint32 advance = SerialPackage::unpack32bit(package.mid(4, 6));

    switch (command) {
    case Microtome::Stepper::Feedrate::motorControl:
        emit advancePosition(advance);
        reportStatus("stepper feedrate position " + QString::number(advance));
        break;
    case Microtome::Stepper::Feedrate::sendPosition:
        reportStatus("stepper feedrate auto position " + package);
        break;
    case Microtome::Stepper::Feedrate::feed:
        reportStatus("stepper set feed rate " + QString::number(feedstep) + " [nm]");
        break;
    case Microtome::Stepper::NorthSouthStage::motorControl:
        reportStatus("stepper NorthSouthStage motor control " + package);
        break;
    case Microtome::Stepper::NorthSouthStage::sendPosition:
        reportStatus("stepper NorthSouthStage position " + package);
        break;
    case Microtome::Stepper::EastWestStage::motorControl:
        reportStatus("stepper EastWestStage motor control " + package);
        break;
    case Microtome::Stepper::EastWestStage::sendPosition:
        reportStatus("stepper EastWestStage position " + package);
        break;
    default:
        reportError("stepper respond is invalid");
    }
}


void MicrotomeDriver::parseCutterRespond(const QString &package)
{
    quint8 command = SerialPackage::unpack8bit(package.mid(0, 2));
    switch (command) {
    case Microtome::Cutter::motor:
        parseCutterMotorRespond(package.mid(2, package.size() - 2));
        break;
    case Microtome::Cutter::cuttingSpeed:
        parseCutterSpeedRespond(package.mid(2, package.size() - 2), "cutting");
        break;
    case Microtome::Cutter::returnSpeed:
        parseCutterSpeedRespond(package.mid(2, package.size() - 2), "return");
        break;
    case Microtome::Cutter::handwheelPosition:
        parseCutterHandwheelRespond(package.mid(2, package.size() - 2));
        break;
    default:
        reportError("cutter respond is invalid " + package);
    }
}


void MicrotomeDriver::parseCutterMotorRespond(const QString &package)
{
    quint8 code = SerialPackage::unpack8bit(package.mid(2, 2));
    switch (code) {
    case Microtome::Status::null:
        reportStatus("cutter motor is off");
        emit cuttingMotorStateChanged(false);
        break;
    case Microtome::Status::set:
        reportStatus("cutter motor is on");
        emit cuttingMotorStateChanged(true);
        break;
    case Microtome::Status::invalidCalibration:
        reportError("cutter motor invalid calibration");
        break;
    default:
        reportError("cutter motor respond is invalid");
    }
}


void MicrotomeDriver::parseCutterSpeedRespond(const QString &package, const QString &tag)
{
    quint8 code = SerialPackage::unpack8bit(package.mid(0, 2));
    quint16 value = SerialPackage::unpack16bit(package.mid(2, 4));
    quint32 speed = static_cast<quint32>(value) * 10;
    if ((code == Microtome::Status::set) || (code == Microtome::Status::get))
        reportStatus("cutter set " + tag + " speed " + QString::number(speed) + " [um/sec]");
    else
        reportError("cutter " + tag + " speed is invalid");
}


void MicrotomeDriver::parseCutterHandwheelRespond(const QString &package)
{
    quint8 status = SerialPackage::unpack8bit(package.mid(0, 2));
    quint8 state = SerialPackage::unpack8bit(package.mid(2, 2));
    if (status == Microtome::Status::get) {

        switch (state) {
        case Microtome::Cutter::Handwheel::retract:
            emit sectionDone();
            emit handwheelPositionChanged("retract");
            reportStatus("cutter handwheel position retract");
            reportStatus("section done");
            break;
        case Microtome::Cutter::Handwheel::beforeWidnow:
            emit handwheelPositionChanged("before cutting window");
            reportStatus("cutter handwheel position before window");
            break;
        case Microtome::Cutter::Handwheel::inWindow:
            emit handwheelPositionChanged("in cutting window");
            reportStatus("cutter handwheel position in window");
            break;
        case Microtome::Cutter::Handwheel::afterWindow:
            emit handwheelPositionChanged("after cutting window");
            reportStatus("cutter handwheel position after window");
            break;
        case Microtome::Status::invalidCalibration:
            reportError("cutter handwheel invalid calibration");
            break;
        default:
            reportError("cutter handwheel position is invalid");
        }

    }
    else
        reportError("cutter handwheel respond is invalid");
}


void MicrotomeDriver::reportStatus(const QString &message)
{
    emit notify("Microtome :: " + message);
}


void MicrotomeDriver::reportError(const QString &message)
{
    emit notify("Microtome :: error, " + message);
}


void MicrotomeDriver::requestCutterSpeed(int value, quint8 code, const QString &tag)
{
    // 1 digit = 10 um/sec
    quint16 valueSpeed = static_cast<quint16>(value / 10);

    QString command = SerialPackage::pack8bit(Microtome::Cutter::address) +
                      SerialPackage::pack8bit(code) +
                      SerialPackage::pack8bit(Microtome::Status::set) +
                      SerialPackage::pack16bit(valueSpeed);
    QString package = SerialPackage::package(command);
    emit schedule(package.toUtf8());
    reportStatus("cutter request " + tag + " speed " + QString::number(value) + " [um/sec]");
}


void MicrotomeDriver::on_readyPackage(const QByteArray &package)
{
    // parse and validate package
    QString buffer = SerialPackage::parse(QString(package));

    if (buffer.size() == 0)
        return;

    if (SerialPackage::isTruncated(buffer)) {
        reportError("respond message truncated " + QString(buffer));
        return;
    }

    if (!SerialPackage::isChecksumValid(buffer)) {
        reportError("respond message bad checksum " + QString(buffer));
        return;
    }

    // process
    reportStatus("respond " + QString(buffer));
    quint8 address = SerialPackage::unpack8bit(buffer.mid(0, 2));
    buffer = buffer.mid(2, buffer.size() - 4); // remove address and checksum from buffer;
    switch (address) {
    case Microtome::System::respond:
        parseSystemRespond(buffer);
        break;
    case Microtome::Stepper::respond:
        parseStepperRespond(buffer);
        break;
    case Microtome::Cutter::respond:
        parseCutterRespond(buffer);
        break;
    default:
        reportError("respond message address is invalid " + QString(buffer));
    }
}


void MicrotomeDriver::on_cuttingSpeedChanged(int value)
{
    requestCutterSpeed(value, Microtome::Cutter::cuttingSpeed, "cutting");
}


void MicrotomeDriver::on_returnSpeedChanged(int value)
{
    requestCutterSpeed(value, Microtome::Cutter::returnSpeed, "return");
}


void MicrotomeDriver::on_feedRateChanged(int value)
{
    // 1 digit = 1 nm
    quint16 valueStep = static_cast<quint16>(value);

    QString command = SerialPackage::pack8bit(Microtome::Stepper::address) +
                      SerialPackage::pack8bit(Microtome::Stepper::Feedrate::feed) +
                      SerialPackage::pack8bit(Microtome::Status::set) +
                      SerialPackage::pack16bit(valueStep);
    QString package = SerialPackage::package(command);
    emit schedule(package.toUtf8());
    reportStatus("stepper request feed rate " + QString::number(value) + " [nm]");
}


void MicrotomeDriver::on_cuttingMotorStateChanged(bool state)
{
    quint8 motorState = Microtome::Status::null;
    QString motorTag = "stop";

    if (state) {
        motorState = Microtome::Status::set;
        motorTag = "start";
    }

    QString command = SerialPackage::pack8bit(Microtome::Cutter::address) +
                      SerialPackage::pack8bit(Microtome::Cutter::motor) +
                      SerialPackage::pack8bit(motorState);
    QString package = SerialPackage::package(command);
    emit schedule(package.toUtf8());
    reportStatus("cutter request motor " + motorTag);
}


void MicrotomeDriver::on_handshake()
{
    QString handshake = SerialPackage::pack8bit(Microtome::System::address) +
                        SerialPackage::pack8bit(Microtome::System::getPartId);
    emit schedule(SerialPackage::package(handshake).toUtf8());
    reportStatus("handshake request part id");

    handshake = SerialPackage::pack8bit(Microtome::System::address) +
                SerialPackage::pack8bit(Microtome::System::login);
    emit schedule(SerialPackage::package(handshake).toUtf8());
    reportStatus("handshake request login");

    handshake = SerialPackage::pack8bit(Microtome::System::address) +
                SerialPackage::pack8bit(Microtome::System::getVersion);
    emit schedule(SerialPackage::package(handshake).toUtf8());
    reportStatus("handshake request version");
}


void MicrotomeDriver::on_aboutToQuit()
{
    on_cuttingMotorStateChanged(false);
    //QMetaObject::invokeMethod(m_serialDriver, "close", Qt::QueuedConnection);
}
