#ifndef MICROTOMEDRIVER_H
#define MICROTOMEDRIVER_H

#include <QObject>

#include "microtomelayout.h"
#include "serialpackage.h"
#include "serialdriver.h"

class MicrotomeDriver : public QObject
{
    Q_OBJECT
public:
    explicit MicrotomeDriver(QObject *parent = nullptr);
    void open(const QString &port, int baudrate);

private:
    SerialDriver *m_serialDriver;

    void parseSystemRespond(const QString &package);
    void parseSystemErrorRespond(const QString &package);
    void parseStepperRespond(const QString &package);
    void parseCutterRespond(const QString &package);
    void parseCutterMotorRespond(const QString &package);
    void parseCutterSpeedRespond(const QString &package, const QString &tag);
    void parseCutterHandwheelRespond(const QString &package);

    void reportStatus(const QString &message);
    void reportError(const QString &message);

    void requestCutterSpeed(int value, quint8 code, const QString &tag);

public slots:
    void on_cuttingSpeedChanged(int value);
    void on_returnSpeedChanged(int value);
    void on_feedRateChanged(int value);
    void on_cuttingMotorStateChanged(bool state);
    void on_handshake();
    void on_aboutToQuit();

private slots:
    void on_readyPackage(const QByteArray &package);

signals:
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
    void handwheelPositionChanged(const QString &position);
    void sectionDone();
    void cuttingMotorStateChanged(bool state);
    void advancePosition(int value);
    void schedule(const QByteArray &package);
};

#endif // MICROTOMEDRIVER_H
