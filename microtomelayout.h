#ifndef MICROTOMELAYOUT_H
#define MICROTOMELAYOUT_H

#include <QtGlobal>

namespace Microtome {

    namespace System {
        static const quint8 address = 0x81;
        static const quint8 respond = 0x18;
        static const quint8 softwareReset = 0xF0;
        static const quint8 getPartId = 0xF1;
        static const quint8 login = 0xF2;
        namespace Error {
            static const quint8 command = 0xF3;
            static const quint8 bufferOverflow = 0x00;
            static const quint8 unknownCommand = 0x03;
            static const quint8 badParameter = 0x07;
            static const quint8 badChecksum = 0x0C;
            static const quint8 commandTruncated = 0x0D;
        }
        static const quint8 getVersion = 0xF5;
    }

    namespace Stepper {
        static const quint8 address = 0x41;
        static const quint8 respond = 0x14;

        namespace Feedrate {
            static const quint8 motorControl = 0x20;
            static const quint8 sendPosition = 0x21;
            static const quint8 feed = 0x23;
        }

        namespace NorthSouthStage {
            static const quint8 motorControl = 0x30;
            static const quint8 sendPosition = 0x31;
        }

        namespace EastWestStage {
            static const quint8 motorControl = 0x40;
            static const quint8 sendPosition = 0x41;
        }
    }

    namespace Cutter {
        static const quint8 address = 0x51;
        static const quint8 respond = 0x15;
        static const quint8 motor = 0x20;
        static const quint8 cuttingSpeed = 0x30;
        static const quint8 returnSpeed = 0x31;
        static const quint8 handwheelPosition = 0x40;

        namespace Handwheel {
            static const quint8 retract = 0x00;
            static const quint8 beforeWidnow = 0x01;
            static const quint8 afterWindow = 0x02;
            static const quint8 inWindow = 0x03;
        }
    }

    namespace Status {
        static const quint8 null = 0x00;
        static const quint8 set = 0x01;
        static const quint8 get = 0xFF;
        static const quint8 invalidCalibration = 0xE0;
    }

}


#endif // MICROTOMELAYOUT_H
