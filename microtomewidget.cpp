#include "microtomewidget.h"
#include "ui_microtomewidget.h"

const int returnSpeedLow = 10000;
const int returnSpeedMedium = 20000;
const int returnSpeedHigh = 30000;
const int cuttingSpeedMax = 10000;
const int feedStepMax = 10000;


MicrotomeWidget::MicrotomeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MicrotomeWidget),
    m_timer(new QTimer(this)),
    m_requestStop(false),
    m_requestRecording(false),
    m_ticks(0),
    m_sections(0),
    m_cuts(0),
    m_advance(0),
    m_progress(0),
    m_cuttingSpeed(0),
    m_returnSpeed(0),
    m_feedRate(0),
    m_feedRateTotal(0)
{
    ui->setupUi(this);

    m_timer->setInterval(1000);
    connect(m_timer, &QTimer::timeout, this, &MicrotomeWidget::on_timerTick);
    connect(ui->pushButton_snapshot, &QPushButton::clicked, this, &MicrotomeWidget::snap);

    ui->spinBox_cuttingSpeed->setMaximum(cuttingSpeedMax);
    ui->spinBox_feedStep->setMaximum(feedStepMax);
    ui->spinBox_cuttingSpeed->setKeyboardTracking(false);
    ui->spinBox_feedStep->setKeyboardTracking(false);
}

MicrotomeWidget::~MicrotomeWidget()
{
    if (m_timer->isActive())
        m_timer->stop();

    delete ui;
}


void MicrotomeWidget::setCuttingSpeed(int value)
{
    m_cuttingSpeed = value;
    ui->spinBox_cuttingSpeed->setValue(m_cuttingSpeed);
    emit cuttingSpeedChanged(m_cuttingSpeed);
}


void MicrotomeWidget::setReturnSpeed(int value)
{
    if (value <= returnSpeedLow)
        m_returnSpeed = returnSpeedLow;
    else if ((returnSpeedLow < value) | (value <= returnSpeedMedium))
        m_returnSpeed = getClosest(returnSpeedLow, returnSpeedMedium, value);
    else if ((returnSpeedMedium < value) | (value <= returnSpeedHigh))
        m_returnSpeed = getClosest(returnSpeedMedium, returnSpeedHigh, value);
    else
        m_returnSpeed = returnSpeedHigh;

    emit returnSpeedChanged(m_returnSpeed);

    int index = 0;
    if (m_returnSpeed == returnSpeedMedium)
        index = 1;
    else if (m_returnSpeed == returnSpeedHigh)
        index = 2;

    ui->comboBox_returnSpeed->setCurrentIndex(index);
}


void MicrotomeWidget::setFeedRate(int value)
{
    m_feedRate = value;
    ui->spinBox_feedStep->setValue(m_feedRate);
}


void MicrotomeWidget::setSections(int sections, int cuts)
{
    m_sections = sections;
    m_cuts = cuts;
    ui->lineEdit_sections->setText(QString::number(m_cuts) + " / " + QString::number(m_sections));
    emit sectionsChanged(m_sections);
}


void MicrotomeWidget::setAdvance(int advance, int progress, int feedRateTotal)
{
    m_advance = advance;
    m_progress = progress;
    m_feedRateTotal = feedRateTotal;

    qreal fracAdvance = 100.0 * m_advance / m_feedRateTotal;
    qreal fracProgress = 100.0 * m_progress / m_feedRateTotal;
    QString fracText = QString::number(fracProgress, 'f', 2) + " / " +
                       QString::number(fracAdvance, 'f', 2);
    ui->lineEdit_advance->setText(fracText);
    emit advanceChanged(m_advance);
}


int MicrotomeWidget::getClosest(int valueLeft, int valueRight, int value)
{
    if ((value - valueLeft) >= (valueRight - value))
        return valueRight;
    else
        return valueLeft;
}


void MicrotomeWidget::cuttingMotorStart()
{
    // reset values
    //m_cuts = 0;
    //m_progress = 0;

    // arm timer
    m_ticks = 0;
    ui->lineEdit_elapsed->setText("00:00:00");
    m_timer->start();

    emit cuttingSpeedChanged(m_cuttingSpeed);
    emit returnSpeedChanged(m_returnSpeed);
    emit feedRateChanged(m_feedRate);
    emit cuttingMotor(true);
    ui->pushButton_slice->setEnabled(false);
    //ui->pushButton_cut->setEnabled(false);
}


void MicrotomeWidget::cuttingMotorStop()
{
    m_timer->stop();
    emit cuttingMotor(false);
    ui->pushButton_slice->setEnabled(true);
    ui->pushButton_cut->setEnabled(true);
}


void MicrotomeWidget::on_handwheelPositionChanged(const QString &tag)
{
    ui->lineEdit_state->setText(tag);

    if (tag == "retract") {

        if (m_requestStop) {
            m_requestStop = false;
            cuttingMotorStop();
        }

    }

}


void MicrotomeWidget::on_sectionsChannged()
{
    setSections(m_sections + 1, m_cuts + 1);
}


void MicrotomeWidget::on_advanceChanged(int value)
{
    setAdvance(m_advance + value, m_progress + value);
}


void MicrotomeWidget::on_cuttingMotorStateChanged(bool state)
{
    if (!state) {
        emit enableEventsLog(false);
        emit enableVideoLog(false);
    }
}


void MicrotomeWidget::on_spinBox_cuttingSpeed_valueChanged(int value)
{
    m_cuttingSpeed = value;
    emit cuttingSpeedChanged(m_cuttingSpeed);
}


void MicrotomeWidget::on_spinBox_feedStep_valueChanged(int value)
{
    m_feedRate = value;
    emit feedRateChanged(m_feedRate);
}


void MicrotomeWidget::on_comboBox_returnSpeed_activated(const QString &arg)
{
    if (arg == "low") {
        m_returnSpeed = returnSpeedLow;
    }
    else if (arg == "medium") {
        m_returnSpeed = returnSpeedMedium;
    }
    else if (arg == "high") {
        m_returnSpeed = returnSpeedHigh;
    }
    emit returnSpeedChanged(m_returnSpeed);
}


void MicrotomeWidget::on_checkBox_video_stateChanged(int value)
{
    if (value == Qt::Unchecked) {
        m_requestRecording = false;
        if (m_timer->isActive())
            emit enableVideoLog(false);
    }
    else if (value == Qt::Checked) {
        m_requestRecording = true;
        if (m_timer->isActive())
            emit enableVideoLog(true);
    }

}


void MicrotomeWidget::on_pushButton_reset_clicked()
{
    setSections(0, 0);
    setAdvance(0, 0);
}


void MicrotomeWidget::on_pushButton_slice_clicked()
{
    cuttingMotorStart();
    m_requestStop = true;
    ui->pushButton_cut->setEnabled(false);
}


void MicrotomeWidget::on_pushButton_cut_clicked()
{
    if (ui->pushButton_cut->text() == "cut") {
        ui->pushButton_cut->setText("stop");
        cuttingMotorStart();
        emit enableEventsLog(true);
        if (m_requestRecording)
            emit enableVideoLog(true);
    }
    else {
        ui->pushButton_cut->setText("cut");
        m_requestStop = true;
    }
}


void MicrotomeWidget::on_timerTick()
{
    m_ticks++;
    QString textTime = QDateTime::fromSecsSinceEpoch(m_ticks).toUTC().toString("hh:mm:ss");
    ui->lineEdit_elapsed->setText(textTime);
}
