#ifndef MICROTOMEWIDGET_H
#define MICROTOMEWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QDateTime>
#include <QDebug>

namespace Ui {
class MicrotomeWidget;
}

class MicrotomeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MicrotomeWidget(QWidget *parent = nullptr);
    ~MicrotomeWidget();

    void setCuttingSpeed(int value);
    void setReturnSpeed(int value);
    void setFeedRate(int value);
    void setSections(int sections, int cuts = 0);
    void setAdvance(int advance, int progress = 0, int feedRateTotal = 20000);

private:
    Ui::MicrotomeWidget *ui;
    QTimer *m_timer;
    bool m_requestStop;
    bool m_requestRecording;
    int m_ticks;
    int m_sections;
    int m_cuts;
    int m_advance;
    int m_progress;
    int m_cuttingSpeed;
    int m_returnSpeed;
    int m_feedRate;
    int m_feedRateTotal;

    int getClosest(int valueLeft, int valueRight, int value);
    void cuttingMotorStart();
    void cuttingMotorStop();

public slots:
    void on_handwheelPositionChanged(const QString &tag);
    void on_sectionsChannged();
    void on_advanceChanged(int value);
    void on_cuttingMotorStateChanged(bool state);

private slots:
    void on_spinBox_cuttingSpeed_valueChanged(int value);
    void on_spinBox_feedStep_valueChanged(int value);
    void on_comboBox_returnSpeed_activated(const QString &arg);
    void on_checkBox_video_stateChanged(int value);
    void on_pushButton_reset_clicked();
    void on_pushButton_slice_clicked();
    void on_pushButton_cut_clicked();

    void on_timerTick();

signals:
    void cuttingSpeedChanged(int value);
    void returnSpeedChanged(int value);
    void feedRateChanged(int value);
    void sectionsChanged(int value);
    void advanceChanged(int value);
    void cuttingMotor(bool state);
    void enableVideoLog(bool state);
    void enableEventsLog(bool state);
    void snap();
};

#endif // MICROTOMEWIDGET_H
