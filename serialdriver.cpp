#include "serialdriver.h"

SerialDriver::SerialDriver(QObject *parent) : QObject(parent),
    m_serialPort(new QSerialPort(this)),
    m_timerWrite(new QTimer(this)),
    m_timerRead(new QTimer(this)),
    m_isWriting(false),
    m_isReading(false),
    m_packageEnd(0x03),
    m_timeoutDuration(5000),
    m_bytesWritten(0)
{
    connect(m_serialPort, &QSerialPort::readyRead, this, &SerialDriver::on_readyRead);
    connect(m_serialPort, &QSerialPort::bytesWritten, this, &SerialDriver::on_bytesWritten);
    connect(m_serialPort, &QSerialPort::errorOccurred, this, &SerialDriver::on_error);
    connect(m_timerWrite, &QTimer::timeout, this, &SerialDriver::on_timeout);
    connect(m_timerRead, &QTimer::timeout, this, &SerialDriver::on_timeout);

    m_timerWrite->setSingleShot(true);
    m_timerRead->setSingleShot(true);
}


void SerialDriver::open(const QString &port, int baudrate)
{
    m_serialPort->setPortName(port);
    m_serialPort->setBaudRate(baudrate);
    m_serialPort->setDataBits(QSerialPort::Data8);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->setFlowControl(QSerialPort::NoFlowControl);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    m_serialPort->open(QIODevice::ReadWrite);

    if (m_serialPort->isOpen())
        driverNotify("device is connected");
    else
        driverNotify("device is missing", true);
}


void SerialDriver::close()
{
    if (!m_serialPort->isOpen())
        return;

    m_serialPort->close();
    driverNotify("device is closed");
}


void SerialDriver::writeToBuffer(const QByteArray &package)
{
    m_bufferWrite = package;

    if (!m_serialPort->isOpen()) {
        driverNotify("device not open", true);
        return;
    }

    m_serialPort->write(m_bufferWrite);
    m_isWriting = true;
    m_bytesWritten = 0;
    m_timerWrite->start(m_timeoutDuration);
}


void SerialDriver::driverNotify(const QString &note, bool isError)
{
    QString message = QString("SerialPort :: %1 at port %2, baud rate %3")
                      .arg(note)
                      .arg(m_serialPort->portName())
                      .arg(m_serialPort->baudRate());
    Qt::GlobalColor color = Qt::gray;

    if (isError) {
        message += ", error " + m_serialPort->errorString();
        color = Qt::red;
    }

    emit notify(message, color);
}


void SerialDriver::on_schedule(const QByteArray &package)
{
    if (m_isWriting)
        m_schedular.append(package);
    else
        writeToBuffer(package);
}


void SerialDriver::on_bytesWritten(qint64 bytes)
{
    m_bytesWritten += bytes;

    if (m_bytesWritten == -1) {
        driverNotify("failed to write data", true);
        m_isWriting = false;
        return;
    }
    else if (m_bytesWritten < m_bufferWrite.size()) // continue writing
        return;
    else if (m_bytesWritten == m_bufferWrite.size()) {
        driverNotify(QString(m_bufferWrite).trimmed() + " " + QString::number(m_bytesWritten) + " bytes were succsessfully written");
        m_timerWrite->stop();
    }

    // check for next message
    if (m_schedular.isEmpty())
        m_isWriting = false;
    else
        writeToBuffer(m_schedular.takeFirst());
}


void SerialDriver::on_readyRead()
{
    if (!m_isReading) {
        m_timerRead->start(m_timeoutDuration);
        m_isReading = true;
    }

    m_bufferRead += m_serialPort->readAll();
    int indexEnd = m_bufferRead.indexOf(m_packageEnd) + 1;
    if (indexEnd > 0) {
        emit readyPackage(m_bufferRead.mid(0, indexEnd));
        m_bufferRead.remove(0, indexEnd);
        m_timerRead->stop();
        m_isReading = false;
    }
}


void SerialDriver::on_error(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ReadError)
        driverNotify("an I/O error occured while reading data", true);
    else if (error == QSerialPort::WriteError)
        driverNotify("an I/O error occured while writing data", true);
}


void SerialDriver::on_timeout()
{
    driverNotify("operation timed out", true);
}
