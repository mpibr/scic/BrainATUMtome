#include "serialpackage.h"

bool SerialPackage::isTruncated(const QString &buffer)
{
    return (buffer.size() % 2 != 0);
}


bool SerialPackage::isChecksumValid(const QString &buffer)
{
    quint8 checksumReceived = unpack8bit(buffer.mid(buffer.size() - 2, 2));
    quint8 checksumLocal = checksum(buffer.mid(0, buffer.size() - 2));
    return (checksumReceived == checksumLocal);
}


quint8 SerialPackage::checksum(const QString &buffer)
{
    quint8 valueChecksum = 0;
    for (int i = 0; i < buffer.size(); i += 2)
        valueChecksum += unpack8bit(buffer.mid(i, 2));
    valueChecksum = ~valueChecksum + 1;
    return valueChecksum;
}


quint8 SerialPackage::unpack8bit(const QString &buffer)
{
    return static_cast<quint8>(buffer.toUInt(nullptr, 16));
}


quint16 SerialPackage::unpack16bit(const QString &buffer)
{
    return static_cast<quint16>(buffer.toUInt(nullptr, 16));
}


quint32 SerialPackage::unpack32bit(const QString &buffer)
{
    return static_cast<quint32>(buffer.toUInt(nullptr, 16));
}


QString SerialPackage::parse(const QString &buffer)
{
    QString localbuffer = "";
    for (int i = 0; i < buffer.size(); ++i) {
        if ((buffer.at(i) >= QChar('A') && buffer.at(i) <= QChar('F')) ||
            (buffer.at(i) >= QChar('a') && buffer.at(i) <= QChar('f')) ||
            (buffer.at(i) >= QChar('0') && buffer.at(i) <= QChar('9')))
            localbuffer.append(buffer.at(i));
    }
    return localbuffer.toUpper();
}


QString SerialPackage::package(const QString &buffer)
{
    quint8 checksumValue = checksum(buffer);
    QString checksumText = pack8bit(checksumValue);
    return '!' + buffer + checksumText + '\r';
}


QString SerialPackage::pack8bit(quint8 value)
{
    return QString("%1").arg(value, 2, 16, QChar('0')).toUpper();
}


QString SerialPackage::pack16bit(quint16 value)
{
    return QString("%1").arg(value, 4, 16, QChar('0')).toUpper();
}


QString SerialPackage::pack32bit(quint32 value)
{
    return QString("%1").arg(value, 8, 16, QChar('0')).toUpper();
}
