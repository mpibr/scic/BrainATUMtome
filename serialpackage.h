#ifndef SERIALPACKAGE_H
#define SERIALPACKAGE_H

#include <QString>
#include <QDebug>

namespace SerialPackage
{
    bool isTruncated(const QString &buffer);
    bool isChecksumValid(const QString &buffer);
    quint8 checksum(const QString &buffer);
    quint8 unpack8bit(const QString &buffer);
    quint16 unpack16bit(const QString &buffer);
    quint32 unpack32bit(const QString &buffer);
    QString parse(const QString &buffer);
    QString package(const QString &buffer);
    QString pack8bit(quint8 value);
    QString pack16bit(quint16 value);
    QString pack32bit(quint32 value);
};

#endif // SERIALPACKAGE_H
