#include "settings.h"

Settings::Settings(QObject *parent) : QObject(parent),
    m_settings(nullptr)
{
    checkPath(pathBase());
    checkPath(pathLogs());
    checkPath(pathVideos());
    checkPath(pathSettings());
}


void Settings::open(const QString &fileName)
{
    QString fileIni = QDir(pathSettings()).filePath(fileName);

    QFileInfo fileCheck(fileIni);
    if (!(fileCheck.exists() && fileCheck.isFile())) {
        emit notify("Settings :: config file is missing, revert to default parameters", Qt::red);
        loadDefaultSettings();
        return;
    }

    m_settings =  new QSettings(fileIni, QSettings::IniFormat, this);
}


QString Settings::eposLowerPort() const
{
    return m_settings->value("epos/lowerPort", defaultEposLowerPort()).toString();
}


QString Settings::eposUpperPort() const
{
    return m_settings->value("epos/upperPort", defaultEposUpperPort()).toString();
}


int Settings::eposLowerVelocity() const
{
    return m_settings->value("epos/lowerVelocity", defaultEposLowerVelocity()).toInt();
}


int Settings::eposUpperVelocity() const
{
    return m_settings->value("epos/upperVelocity", defaultEposUpperVelocity()).toInt();
}


QString Settings::microtomePort() const
{
    return m_settings->value("microtome/port", defaultMicrotomePort()).toString();
}


int Settings::microtomeBaudRate() const
{
    return m_settings->value("microtome/baudRate", defaultMicrotomeBaudRate()).toInt();
}


int Settings::microtomeCuttingSpeed() const
{
    return m_settings->value("microtome/cuttingSpeed", defaultMicrotomeCuttingSpeed()).toInt();
}


int Settings::microtomeReturnSpeed() const
{
    return m_settings->value("microtome/returnSpeed", defaultMicrotomeReturnSpeed()).toInt();
}


int Settings::microtomeFeedStep() const
{
    return m_settings->value("microtome/feedStep", defaultMicrotomeFeedStep()).toInt();
}


int Settings::microtomeFeedRateTotal() const
{
    return m_settings->value("microtome/feedRateTotal", defaultMicrotomeFeedRateTotal()).toInt();
}


int Settings::microtomeSections() const
{
    return m_settings->value("microtome/sections", defaultMicrotomeSections()).toInt();
}


int Settings::microtomeAdvance() const
{
    return m_settings->value("microtome/advance", defaultMicrotomeAdvance()).toInt();
}


QString Settings::syringePort() const
{
    return m_settings->value("syringe/port", defaultSyringePort()).toString();
}


int Settings::syringeBaudRate() const
{
    return m_settings->value("syringe/baudRate", defaultSyringeBaudRate()).toInt();
}


int Settings::cameraId() const
{
    return m_settings->value("camera/id", defaultCameraId()).toInt();
}


int Settings::cameraFrameRate() const
{
    return m_settings->value("camera/frameRate", defaultCameraFrameRate()).toInt();
}


void Settings::checkPath(const QString &path)
{
    QDir dir(path);
        if (!dir.exists())
            dir.mkdir(path);
}


void Settings::loadDefaultSettings()
{
    QString fileSettings = QDir(pathSettings()).filePath("settings.txt");
    m_settings =  new QSettings(fileSettings, QSettings::IniFormat, this);

    // application
    m_settings->setValue("application/name", defaultApplicationName());
    m_settings->setValue("application/organisation", defaultApplicationOrganisation());

    // epos
    m_settings->setValue("epos/deviceName", defaultEposDeviceName());
    m_settings->setValue("epos/lowerPort", defaultEposLowerPort());
    m_settings->setValue("epos/upperPort", defaultEposUpperPort());
    m_settings->setValue("epos/lowerVelocity", defaultEposLowerVelocity());
    m_settings->setValue("epos/upperVelocity", defaultEposUpperVelocity());
    m_settings->setValue("epos/mmTapeMinDivRpm", defaultEposMmTapePerRotation());

    // microtome
    m_settings->setValue("microtome/deviceName", defaultMicrotomeDeviceName());
    m_settings->setValue("microtome/port", defaultMicrotomePort());
    m_settings->setValue("microtome/baudRate", defaultMicrotomeBaudRate());
    m_settings->setValue("microtome/cuttingSpeed", defaultMicrotomeCuttingSpeed());
    m_settings->setValue("microtome/returnSpeed", defaultMicrotomeReturnSpeed());
    m_settings->setValue("microtome/feedStep", defaultMicrotomeFeedStep());
    m_settings->setValue("microtome/feedRateTotal", defaultMicrotomeFeedRateTotal());
    m_settings->setValue("microtome/sections", defaultMicrotomeSections());
    m_settings->setValue("microtome/advance", defaultMicrotomeAdvance());

    // syringe
    m_settings->setValue("syringe/deviceName", defaultSyringeDeviceName());
    m_settings->setValue("syringe/port", defaultSyringePort());
    m_settings->setValue("syringe/baudRate", defaultSyringeBaudRate());

    // camera
    m_settings->setValue("camera/deviceName", defaultCameraDeviceName());
    m_settings->setValue("camera/id", defaultCameraId());
    m_settings->setValue("camera/frameRate", defaultCameraFrameRate());
}


void Settings::on_lowerVelocityChanged(int value)
{
    m_settings->setValue("epos/lowerVelocity", qAbs(value));
}


void Settings::on_upperVelocityChanged(int value)
{
    m_settings->setValue("epos/upperVelocity", qAbs(value));
}


void Settings::on_cuttingSpeedChanged(int value)
{
    m_settings->setValue("microtome/cuttingSpeed", value);
}


void Settings::on_returnSpeedChanged(int value)
{
    m_settings->setValue("microtome/returnSpeed", value);
}


void Settings::on_feedStepChanged(int value)
{
    m_settings->setValue("microtome/feedStep", value);
}


void Settings::on_sectionsChanged(int value)
{
    m_settings->setValue("microtome/sections", value);
}


void Settings::on_advanceChanged(int value)
{
    m_settings->setValue("microtome/advance", value);
}
