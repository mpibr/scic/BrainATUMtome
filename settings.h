#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QStandardPaths>
#include <QDir>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = nullptr);

    void open(const QString &fileName);
    void save();

    QString pathBase() const {return QDir(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).filePath("BrainATUMtome");}
    QString pathLogs() const {return QDir(pathBase()).filePath("logs");}
    QString pathVideos() const {return QDir(pathBase()).filePath("videos");}
    QString pathSettings() const {return QDir(pathBase()).filePath("settings");}

    QString eposLowerPort() const;
    QString eposUpperPort() const;
    int eposLowerVelocity() const;
    int eposUpperVelocity() const;

    QString microtomePort() const;
    int microtomeBaudRate() const;
    int microtomeCuttingSpeed() const;
    int microtomeReturnSpeed() const;
    int microtomeFeedStep() const;
    int microtomeFeedRateTotal() const;
    int microtomeSections() const;
    int microtomeAdvance() const;

    QString syringePort() const;
    int syringeBaudRate() const;

    int cameraId() const;
    int cameraFrameRate() const;

private:
    QSettings *m_settings;

    void checkPath(const QString &path);
    void loadDefaultSettings();

    QString defaultApplicationName() const {return "BrainATUMtome";}
    QString defaultApplicationOrganisation() const {return "MPIBR";}

    QString defaultEposDeviceName() const {return "EPOS2";}
    QString defaultEposLowerPort() const {return "USB0";}
    QString defaultEposUpperPort() const {return "USB1";}
    int defaultEposLowerVelocity() const {return 1000;}
    int defaultEposUpperVelocity() const {return 1000;}
    qreal defaultEposMmTapePerRotation() const {return 1.0;}

    QString defaultMicrotomeDeviceName() const {return "LeicaEMUC7";}
    QString defaultMicrotomePort() const {return "COM1";}
    int defaultMicrotomeBaudRate() const {return 19200;}
    int defaultMicrotomeCuttingSpeed() const {return 100;}
    int defaultMicrotomeReturnSpeed() const {return 10000;}
    int defaultMicrotomeFeedStep() const {return 30;}
    int defaultMicrotomeFeedRateTotal() const {return 200000;}
    int defaultMicrotomeSections() const {return 0;}
    int defaultMicrotomeAdvance() const {return 0;}

    QString defaultSyringeDeviceName() const {return "Syringe";}
    QString defaultSyringePort() const {return "COM2";}
    int defaultSyringeBaudRate() const {return 9600;}

    QString defaultCameraDeviceName() const {return "Leica";}
    int defaultCameraId() const {return 0;}
    int defaultCameraFrameRate() const {return 20;}


public slots:
    void on_lowerVelocityChanged(int value);
    void on_upperVelocityChanged(int value);
    void on_cuttingSpeedChanged(int value);
    void on_returnSpeedChanged(int value);
    void on_feedStepChanged(int value);
    void on_sectionsChanged(int value);
    void on_advanceChanged(int value);

signals:
    void notify(const QString &message, const Qt::GlobalColor color = Qt::black);

};

#endif // SETTINGS_H
