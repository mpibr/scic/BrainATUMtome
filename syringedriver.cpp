#include "syringedriver.h"

SyringeDriver::SyringeDriver(QObject *parent) : QObject(parent),
    m_serialPort(new SerialDriver(this))
{
    connect(this, &SyringeDriver::schedule, m_serialPort, &SerialDriver::on_schedule);
    connect(m_serialPort, &SerialDriver::readyPackage, this, &SyringeDriver::on_respond);
    connect(m_serialPort, &SerialDriver::notify, this, &SyringeDriver::notify);
}


void SyringeDriver::open(const QString &portName, int baudRate)
{
    m_serialPort->open(portName, baudRate);
    m_serialPort->setPackageEnd(0x03);

    // command set
    // VER
    // RUN
    // STP
    // PUR
}


void SyringeDriver::on_pump()
{
    emit notify("Syringe :: pump");
    emit schedule(package("RUN"));
}


void SyringeDriver::on_respond(const QByteArray &respond)
{
    QString message = "";
    for (int i = 0; i < respond.size(); ++i) {
        if ((respond.at(i) >= QChar('A') && respond.at(i) <= QChar('Z')) ||
            (respond.at(i) >= QChar('a') && respond.at(i) <= QChar('z')) ||
            (respond.at(i) >= QChar('0') && respond.at(i) <= QChar('9')))
            message.append(respond.at(i));
    }
    //QString message = QString(respond).trimmed();
    //int indexTextStart = message.indexOf(0x02);
    //if (indexTextStart > -1)
    //    message.remove(indexTextStart, 1);
    emit notify("Syringe :: respond " + message, Qt::gray);
}


void SyringeDriver::on_aboutToQuit()
{
    emit schedule(package("STP"));
    m_serialPort->close();
}


void SyringeDriver::on_handshake()
{
    emit notify("Syringe :: request VERSION", Qt::gray);
    emit schedule(package("VER"));
}


QByteArray SyringeDriver::package(const QString &message)
{
    QString package = message + '\n' + '\r';
    return package.toUpper().toUtf8();
}
