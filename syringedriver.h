#ifndef SYRINGEDRIVER_H
#define SYRINGEDRIVER_H

#include <QObject>

#include "serialdriver.h"

class SyringeDriver : public QObject
{
    Q_OBJECT

public:
    explicit SyringeDriver(QObject *parent = nullptr);

    void open(const QString &portName, int baudRate);

private:
    SerialDriver *m_serialPort;

    QByteArray package(const QString &command);

public slots:
    void on_pump();
    void on_handshake();
    void on_aboutToQuit();

private slots:
    void on_respond(const QByteArray &respond);

signals:
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
    void schedule(const QByteArray &message);
};

#endif // SYRINGEDRIVER_H
