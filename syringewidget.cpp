#include "syringewidget.h"
#include "ui_syringewidget.h"

SyringeWidget::SyringeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SyringeWidget),
    m_uiTrigger(false),
    m_section(0),
    m_sectionDelta(1),
    m_sectionTrigger(0)
{
    ui->setupUi(this);
}

SyringeWidget::~SyringeWidget()
{
    delete ui;
}


void SyringeWidget::armTrigger()
{
    m_sectionTrigger = m_section + m_sectionDelta;
}


void SyringeWidget::pullTrigger()
{
    emit pump();
    armTrigger();
    ui->lineEdit_history->setText(QString::number(m_section));
}

void SyringeWidget::on_sectionsChannged(int value)
{
    // ui synced trigger
    if (m_uiTrigger) {
        pullTrigger();
        m_uiTrigger = false;
        ui->pushButton_pump->setEnabled(true);
    }

    // auto trigger
    m_section = value;
    if (ui->checkBox_auto->isChecked() && (m_section == m_sectionTrigger))
        pullTrigger();
}


void SyringeWidget::on_pushButton_pump_clicked()
{
    if (ui->checkBox_sync->isChecked()) {
        ui->pushButton_pump->setEnabled(false);
        m_uiTrigger = true;
    }
    else {
        pullTrigger();
    }
}

void SyringeWidget::on_spinBox_sections_valueChanged(int value)
{
    m_sectionDelta = value;
    if (ui->checkBox_auto->isChecked())
        armTrigger();
}

void SyringeWidget::on_checkBox_sync_stateChanged(int value)
{
    if (value == 0) {
        m_uiTrigger = false;
        ui->pushButton_pump->setEnabled(true);
    }

}

void SyringeWidget::on_checkBox_auto_stateChanged(int value)
{
    if (value)
        armTrigger();
}
