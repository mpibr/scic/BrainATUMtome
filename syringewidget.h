#ifndef SYRINGEWIDGET_H
#define SYRINGEWIDGET_H

#include <QWidget>

namespace Ui {
class SyringeWidget;
}

class SyringeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SyringeWidget(QWidget *parent = nullptr);
    ~SyringeWidget();

private:
    Ui::SyringeWidget *ui;
    bool m_uiTrigger;
    int m_section;
    int m_sectionDelta;
    int m_sectionTrigger;

    void armTrigger();
    void pullTrigger();

public slots:
    void on_sectionsChannged(int value);

signals:
    void pump();

private slots:
    void on_pushButton_pump_clicked();
    void on_spinBox_sections_valueChanged(int value);
    void on_checkBox_sync_stateChanged(int value);
    void on_checkBox_auto_stateChanged(int value);
};

#endif // SYRINGEWIDGET_H
