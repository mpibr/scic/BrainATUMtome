#include "viewerwidget.h"

ViewerWidget::ViewerWidget(QWidget *parent) : QGraphicsView(parent)
{
    setScene(&m_scene);
    scene()->addItem(&m_pixmap);
}


void ViewerWidget::on_frame(const cv::Mat &frame_cv)
{
    // create QImage
    QImage frame_qt(frame_cv.data,
                    frame_cv.cols,
                    frame_cv.rows,
                    static_cast<int>(frame_cv.step),
                    QImage::Format_BGR888);

    // update scene
    m_scene.setSceneRect(QRectF(0, 0, frame_cv.cols, frame_cv.rows));

    // update pixmap
    m_pixmap.setPixmap(QPixmap::fromImage(frame_qt));
    fitInView(&m_pixmap, Qt::KeepAspectRatio);
}


void ViewerWidget::on_addroi(bool flag)
{
    if (flag) {
        // m_roi = new RoiRectItem();
        // scene()->addItem(m_roi);
    }
    else {
        // scene()->removeItem(m_roi);
    }
}
