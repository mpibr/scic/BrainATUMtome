#ifndef VIEWERWIDGET_H
#define VIEWERWIDGET_H

#include <QObject>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>

#include <opencv2/opencv.hpp>

class ViewerWidget : public QGraphicsView
{
    Q_OBJECT
public:
    explicit ViewerWidget(QWidget *parent = nullptr);

private:
    QGraphicsScene m_scene;
    QGraphicsPixmapItem m_pixmap;

public slots:
    void on_frame(const cv::Mat &frame_cv);
    void on_addroi(bool flag);
};

#endif // VIEWERWIDGET_H
